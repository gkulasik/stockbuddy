source 'https://rubygems.org'


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.8'

# Use postgresql as the database for Active Record
gem 'pg', '~> 0.18.4'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use jquery as the JavaScript library
gem 'jquery-rails', '~> 4.0.5'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks', '~> 2.5.3'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', '~> 8.2.1'

  # Factories to replace fixtures
  gem 'factory_girl_rails', '~> 4.8.0'
end

group :test do
  # Integration testing - Browser simulation
  gem 'capybara', '~> 2.12.0'

  # Testing helper
  gem 'shoulda', '~> 3.5.0'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'
  # Gem to help get all the Favicon assets set up correctly
  gem 'rails_real_favicon', '~> 0.0.7'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring', '~> 1.6.4'

  # Required for Rails console to work
  gem 'rb-readline', '~> 0.5.4'

  # Auto UML generator
  gem 'railroady'

end

gem 'delayed_job_active_record', '~> 4.1.0'

gem 'tradier', '~> 0.5.6'

gem 'daemons', '~> 1.2.3'

gem 'sms-easy', '~> 1.3.1'

gem 'figaro', '~> 1.0.0'

gem 'foundation-rails', '~> 5.5.3.2'

gem 'devise', '~> 3.5.3'

gem 'kaminari', '~> 1.0.1'

gem 'foundation-icons-sass-rails', '~> 3.0.0'

gem 'sparkpost_rails', '~> 1.4.0'

gem 'rest-client', '~> 2.0.0'

gem 'draper', '~> 2.1.0'

gem 'exception_notification', '~> 4.2.1' # For emailing exception information (error reporting)

gem 'administrate','~> 0.6.0'

gem 'mail_form', '~> 1.6.0'

gem 'recaptcha', '~> 4.3.1', require: 'recaptcha/rails'

group :production do
  gem 'thin', '~> 1.6.4'

  gem 'newrelic_rpm', '~> 3.14.1.311'

  gem 'rails_12factor', '~> 0.0.3'
end


ruby '2.3.4'