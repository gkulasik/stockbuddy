# StockBuddy #

Find the running application at [stockbuddyapp.herokuapp.com](https://stockbuddyapp.herokuapp.com)

### What is it ###

StockBuddy is an application that will alert you to changes in stocks that you are interested in. It features customizable triggers as well as flexible alert options (email and SMS). 

### Setup ###

* Rails 4.2
* Foundation 5
* Heroku
* Tradier used for stock data