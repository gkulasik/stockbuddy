function getAssetInfo(ticker, populaterFunction) {
    $.ajax({
        url: "/query?ticker=" + ticker,
        type: "GET",
        success: function (info) {
           populaterFunction(info);
        },
        error: function (info) {
            null
        }
    });
}

// Populates views/shared/_more_asset_info.html.erb
function populateAssetInfo(info){
    if (info != null) {
        document.getElementById("info_title").innerHTML = info.name + " (" + info.ticker + ")";
        document.getElementById("info_current_price").innerHTML = info.current_price;
        document.getElementById("info_change").innerHTML = info.change;
        document.getElementById("info_change_percentage").innerHTML = info.change_percentage;
        document.getElementById("info_volume").innerHTML = info.volume;
        document.getElementById("info_average_volume").innerHTML = info.average_volume;
        document.getElementById("info_open").innerHTML = info.open;
        document.getElementById("info_high").innerHTML = info.high;
        document.getElementById("info_low").innerHTML = info.low;
        document.getElementById("info_close").innerHTML = info.close;
        document.getElementById("info_previous_close").innerHTML = info.prev_close;
        document.getElementById("info_52_week_high").innerHTML = info.week_52_high;
        document.getElementById("info_52_week_low").innerHTML = info.week_52_low;
        document.getElementById("info_asset_class").innerHTML = info.asset_type;
    }
    else {
        document.getElementById("info_title").innerHTML = "Information not found";
    }
}
