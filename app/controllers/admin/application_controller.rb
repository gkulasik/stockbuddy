module Admin
  class ApplicationController < Administrate::ApplicationController
    before_action :authenticate_admin

    # Validates that a user is signed in and if they are that they are an admin
    # If not signed in then show error to that effect and redirect back to the root_path
    def authenticate_admin
      unless user_signed_in? && current_user.admin
        flash[:alert] = I18n.t('devise.failure.unauthorized')
        redirect_to root_path
      end
    end

    # Custom destroy to avoid hidden admin dashboards from showing up
    # The only change is the redirect_to
    def destroy
      requested_resource.destroy
      flash[:notice] = translate_with_resource("destroy.success")
      redirect_to :back # to prevent from going back to index of hidden dashboards
    end

  end
end
