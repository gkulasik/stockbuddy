module Admin
  class StockAlertsController < Admin::ApplicationController
    # To customize the behavior of this controller,
    # you can overwrite any of the RESTful actions. For example:
    #
    # def index
    #   super
    #   @resources = StockAlert.
    #     page(params[:page]).
    #     per(10)
    # end

    # Need a custom find_resource because of the inheritance with StockAlerts.
    # For validation purposes we need to validate the class as itself, not what it was before.
    # This is only done when there is a type parameter in the stock_alert hash parameter (on Create)
    # Else the default behavior is used
    def find_resource(param)
      if !params[:stock_alert].nil?
        type = params[:stock_alert][:type]
        found = resource_class.find(param)
        found.nil? ? found : found.becomes(type.constantize)
      else
        resource_class.find(param)
      end
    end

  end
end
