module Admin
  class UsersController < Admin::ApplicationController
    # To customize the behavior of this controller,
    # you can overwrite any of the RESTful actions. For example:
    #
    # def index
    #   super
    #   @resources = User.
    #     page(params[:page]).
    #     per(10)
    # end

    # Define a custom finder by overriding the `find_resource` method:
    # def find_resource(param)
    #   User.find_by!(slug: param)
    # end

    # See https://administrate-prototype.herokuapp.com/customizing_controller_actions
    # for more information

    # To avoid an issue when creating a user in the admin area - it also sets the reset and confirmation tokens. There are uniqueness constraints on these fields so they need to be set.
    def create
      resource = resource_class.new(resource_params)
      resource.reset_password_token = Security.generate_random_token(10)
      resource.confirmation_token = Security.generate_random_token(10)
      if resource.save
        redirect_to(
            [namespace, resource],
            notice: translate_with_resource("create.success"),
        )
      else
        render :new, locals: {
            page: Administrate::Page::Form.new(dashboard, resource),
        }
      end
    end
  end
end
