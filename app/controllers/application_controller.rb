class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  ##
  ### CALLBACKS
  ##
  before_action :prepare_exception_notifier # To make sure that the user info is stored for who this happens to
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :set_friendly_forward # Must come before devise (authenticate_user) else request info is lost
  before_action :authenticate_user!
  before_action :set_analytics

  ##
  ### GOOGLE ANALYTICS
  ##

  # Set the google analytics codes/tokens
  # @return [Nil] just sets @variables to determine if google analytics should be enabled
  def set_analytics
    @google_analytics_code = ENV['GOOGLE_ANALYTICS_CODE'] # get from properties
    # Assume lack of a code means that analytics should NOT be on
    @analytics_on = !@google_analytics_code.blank? ? true : false
  end

  # Set whether ReCaptcha should be on or not.
  # Call only in the particular controllers that need it.
  # @return [Nil] Only sets instance variables for captcha
  def set_captcha
    @captcha_on = BOOL_ENV.fetch('RECAPTCHA_ACTIVE')
  end

  ##
  ### DEVISE
  ##

  # Custom configure what Device attributes can be passed (strong parameters)
  # Added to accommodate the custom fields for a User (first_name, last_name, username)
  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:email, :password, :password_confirmation, :remember_me, :first_name, :last_name) }
    devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:login, :username, :email, :password, :remember_me) }
    devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:email, :password, :password_confirmation, :current_password, :first_name, :last_name) }
  end

  # Path to take a user to once they sign in
  # May also redirect to the friendly forward link - saved before redirect
  # If the user is the admin then it takes them to the admin dashboard else to their watched stocks page
  # @param [User] resource_or_scope
  # @return [String] path to go to after sign in
  def after_sign_in_path_for(resource_or_scope)
    # TODO verify that all links and actions go where user expects within the whole site
    if resource_or_scope.admin
      admin_users_path
    else
      session['friendly_forward'] || watched_stocks_path
    end
  end

  private

  ##
  ### Exception Notifier
  ##

  # Saves user information to know for who errors occur
  # Will automatically be added to exception notification via email
  # Can add other information here if want too - will all be added to bottom of email in 'Data' section
  def prepare_exception_notifier
    request.env['exception_notifier.exception_data'] = {
        :current_user => current_user
    }
  end


  ##
  ### Friendly Forward
  ##

  # This method captures the url a user is trying to access.
  # If they are redirected to sign in due to it being protected
  # then it will store the url in the session to be retrieved later by devise for a friendly forward.
  # Ignores devise controllers, nonGET requests, the root path and public pages
  def set_friendly_forward
    # TODO clean this up - maybe 'should friendly forward' method? and add a new 'protected' method to replace skip_authentication
    unless devise_controller? || request.xhr? || !request.get? || request.path == root_path || is_a?(PublicController) || is_a?(ContactsController)
      session['friendly_forward'] = request.url
    end
  end
end
