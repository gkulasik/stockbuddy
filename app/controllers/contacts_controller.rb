class ContactsController < ApplicationController
  skip_before_action :authenticate_user! # Skip auth for contact pages

  # Render contact form
  def new
    @contact = ContactRequest.new
  end

  # Create a contact request from the params
  # Deliver the data to email
  def create
    @contact = ContactRequest.new(params[:contact_request])
    @contact.request = request
    if @contact.deliver
      flash[:notice] = I18n.t('views.contact_requests.success')
      redirect_to public_contact_path
    else
      flash.now[:error] = I18n.t('views.contact_requests.error')
      render :new
    end
  end
end
