class DeviseOverride::RegistrationsController < Devise::RegistrationsController
  before_action :set_captcha
  before_action :check_captcha, only: [:create]

  protected
  # Path to go to after sign up - IF user is active (currently this is not used but it gives us control of here to route automatically active users (like via OAuth) in the future)
  # @param [User] resource
  # @return [String] path to fo to after sign_up
  def after_sign_up_path_for(resource)
    new_session_path(resource)
  end

  # Path to go to after sign up (used by all users that sign up through regular form)
  # Users are inactive due to needing to be confirmed.
  # FOR CONFIRMABLE
  # @param [User] resource
  # @return [String] where to go after sign up AND inactive
  def after_inactive_sign_up_path_for(resource)
    public_tour_path
  end

  private
  # Used to check the captcha for verification before processing any other information.
  # Will not perform verification if captcha is not on for the environment
  def check_captcha
    if @captcha_on && !verify_recaptcha
      self.resource = resource_class.new sign_up_params
      # Add error to the object rather than a flash - keeps errors consistent
      self.resource.errors.add(:base, I18n.t('views.devise.registrations.new.captcha_failure'))
      flash.delete(:recaptcha_error)
      respond_with_navigational(resource) { render :new }
    end
  end
end