class PublicController < ApplicationController
  ##
  ### CALLBACKS
  ##

  skip_before_action :authenticate_user! # Skip authentication for all public pages

  # root_path
  def home
    # TODO homepage redesign - may not need @image_options anymore
    @image_options = {width: 150, height: 150} # default setting for all the images on the home page (to ensure they all take up the same space)
  end

  # Legal text to try and prevent any legal ramifications
  def terms_of_service
  end

  # Current page users are taken too after signing up
  # Details the use of the site as a preview before they confirm their account
  def tour
  end
end
