class StockAlertsController < ApplicationController
  ##
  ### CALL BACKS
  ##

  before_action :set_stock_alert, only: [:edit, :update, :destroy] # Set the @stock_alert variable for these actions
  before_action :compile_watched_stock_select, only: [:new, :edit]
  before_action :preprocess_params, only: [:create, :update]
  skip_before_action :authenticate_user!, only: [:reset_trigger] # Do not require authentication for reset_trigger. Authenticated via the 'reset_token' param.

  ##
  ### CRUD + I (index)
  ##

  # GET /stock_alerts
  # Show stock alerts either filtered by the stock list id (filter by a specific stock (TSLA) and only show alerts for that stock) or show all the alerts a user has.
  # Params
  # stock_list_id - ID of the stock list the stock alerts should be filtered by
  # @return [Array<StockAlert>] stock alerts for the user (possibly filtered)
  def index
    stock_alerts = current_user.get_stock_alerts
    filtering_params(params).each do |key, value|
      stock_alerts = stock_alerts.public_send(key, value) if value.present? && !stock_alerts.empty?
    end
    filtered_stock_alerts = stock_alerts.map { |alert| alert.decorate } # decorate each with its own type of decorator
    @stock_alerts = Kaminari.paginate_array(filtered_stock_alerts).page(params[:page]).per(StockAlert.default_per_page) # uses the paginates_per setting on the model
  end

  # GET /stock_alerts/new
  # Create a new stock alert
  # Prep for creation of stock alerts
  # Params
  # stock_list_id - ID of the stock list to associate this stock alert with
  # @return [StockAlert] newly created stock alert with associated WatchedStock already attached and loaded
  def new
    # When creating a new stock alert a hidden field is populated with the stock_list_id (to associate the stock alert with the correct stock)
    @stock_alert = StockAlert.new(stock_list_id: params[:alert_for])
  end

  # GET /stock_alerts/1/edit
  # Edit a stock alert
  # Params
  # id - ID of the stock alert to work with (populate @stock_alert)
  def edit
    # Don't need to explicitly get the WatchedStock due to set_stock_alert()
  end

  # POST /stock_alerts
  # Create a stock alert
  # TODO check that all translations are showing up correctly
  def create
    @stock_alert = StockAlert.new(stock_alert_params) # filter out disallowed params
    if @stock_alert.save
      # If save success return a success notice, return to stock alerts path (to show the new alert)
      redirect_to stock_alerts_path, notice: t('controller.stock_alerts.flash.create.success')
    else
      # if failed populate the select list
      compile_watched_stock_select
      render :new
    end
  end

  # PATCH/PUT /stock_alerts/1
  # Update existing stock alerts
  # Params
  # id - ID of the stock alert to work with (populate @stock_alert)
  def update
    if @stock_alert.update_with_type_validations(stock_alert_params) # updates based on the updated type not the old type
      # On success redirect to index with success
      redirect_to stock_alerts_path, notice: t('controller.stock_alerts.flash.update.success')
    else
      # redirect to edit on error
      # Don't need to explicitly get the WatchedStock due to set_stock_alert()
      # if failed populate the select list
      compile_watched_stock_select
      render :edit
    end
  end

  # DELETE /stock_alerts/1
  # Delete a stock alert
  # Params
  # id - ID of the stock alert to work with (populate @stock_alert)
  def destroy
    # destroy the stock alert and provide a confirm message
    @stock_alert.destroy
    redirect_to stock_alerts_url, notice: t('controller.stock_alerts.flash.destroy.success')
  end

  ##
  ### CUSTOM ACTIONS
  ##

  # Reset the stock alert so that it can be triggered again and send out alerts
  # This method allows a user to reset the trigger via link and even while not signed in
  # It will validate the user is allowed to reset the trigger by the reset_token
  # Note: the reset token is reset each time the stock alert it triggered (has a different reset token) this means that old links to reset a particular stock alert will not work. This method allows users to reset stock alerts without having to login (email or sms).
  # Idempotent - can use the same link multiple times without side effects (once it is reset once it cannot be reset again - it is just re-saving the same data)
  #
  # Params
  # stock_alert_id - ID of the stock alert to reset
  # reset_token - the token that confirms this user is allowed to reset the stock alert
  def reset_trigger
    # get data
    @stock_alert = StockAlert.find(params[:id])
    @reset_token = params[:reset_token]
    # determine if the reset is valid # Don't accept blank reset_tokens
    is_valid_reset_request = @stock_alert.reset_token == @reset_token && !@reset_token.blank?
    # determine if the user is logged in (where to take them after reset)
    redirect_location = user_signed_in? ? stock_alerts_path : root_path # location to redirect too
    # determine if the reset is successful
    reset_successful = is_valid_reset_request ? @stock_alert.update(triggered: false) : false
    # determine the text to show the user
    reset_text = reset_successful ? t('controller.stock_alerts.actions.reset_trigger.success') : t('controller.stock_alerts.actions.reset_trigger.error')
    # determine the notice type to show the user
    reset_notice = reset_successful ? {notice: reset_text} : {alert: reset_text}
    # redirect and provide correct redirect/popup
    redirect_to redirect_location, reset_notice
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  # Set the stock alert being worked on in one place (reduce code duplication)
  def set_stock_alert
    @stock_alert = StockAlert.find(params[:id])
    @stock = @stock_alert.get_watched_stock
  end

  # Never trust parameters from the scary internet, only allow the white list through. Filters out disallowed params
  # NOTE: add new parameters here (ex. reset_token) when adding new params to StockAlerts
  def stock_alert_params
    params.require(:stock_alert).permit(:stock_list_id, :reset_token, :id, :value, :alert_name, :type)
  end

  def preprocess_params
    alert = StockAlert.new(stock_alert_params)
    alert.preprocess(params)
  end

  # Provides list of params to filter alerts by. Linked to StockAlert model scopes.
  def filtering_params(params)
    params.slice(:with_trigger_status, :alert_for)
  end

  def compile_watched_stock_select
    @stock_list_ids = current_user.get_watched_stocks.collect { |li| [li.watched_stock.name, li.id] }
  end

end
