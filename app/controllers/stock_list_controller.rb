class StockListController < ApplicationController

  # Controller for stock lists
  # Actions: Destroy
  # Lack of other actions due to what we want to allow a user to manage
  # Users Watch stocks through the stock search controller
  # Users can only watch (create) and unwatch (destroy) watched stocks (stock lists)
  # Watching is done through the StockSearchController
  # TODO check that translations working

  # Destroy a stock list
  # Params
  # id - ID of the stock list to destroy
  def destroy
    @stock_list = StockList.find(params[:id])
    if @stock_list.destroy
      flash[:success] = t('controller.stock_lists.flash.destroy.success')
    else
      flash[:error] = t('controller.stock_lists.flash.destroy.error')
    end
    redirect_to root_path
  end
end
