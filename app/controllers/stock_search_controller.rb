class StockSearchController < ApplicationController
  # Search for a stock
  # Uses the StockDataInterface (which connects to the stock data API we use) to retrieve the data on a particular stock
  # Params
  # q - query string to search for
  # @return [Array<Hash>] with data on the stock (semi raw from API) or empty array if no results
  def search
    if params.has_key?(:q)
      @symbols = StockDataInterface.search(params[:q])
    else
      @symbols = []
    end
  end

  # TODO Test
  # Get information on an asset/company and return JSON for frontend
  # Takes :ticker param for now to search for stock data
  # @return [String] JSON data of the WatchedStockDecorator
  def info_query
    result = if params.has_key?(:ticker)
               exists = WatchedStock.find_by_ticker(params[:ticker])
               stock = if exists.nil?
                         WatchedStock.new_from_stock_data(StockDataInterface.get_quotes(params[:ticker]).first)
                       else
                         exists
                       end
               stock.decorate
             else
               nil
             end
    render :json => result
  end

  #TODO verify that translations are working
  # Design note: though this method creates a stock list it also creates a watched stock and uses search data. It makes it difficult to decide on a 'best' place to have this method. As a result it was determined to stay in the SearchController mainly because it was already here and in the end the method renders search.
  # Add a stock to the watch list (stock list) for a user
  # In reality this method adds a stock to the WatchedStock table, a table that is used by all users to prevent duplication of data. This method adds a new WatchedStock or finds an existing record.
  # This method will query data for the stock to watch, create a WatchedStock if needed, then create a StockList to match the user to the WatchedStock (if it does not already exist)
  # Params
  # to_watch - String - Ticker of stock to watch
  def add_watched_stock
    if params.has_key?(:to_watch)
      watch = params[:to_watch]
      data = StockDataInterface.get_quotes([watch]).first
      stock = WatchedStock.init_from_stock_data(data).find_or_create_by(ticker: watch)
      if stock.errors.empty? && StockList.find_or_create_by(user_id: current_user.id, watched_stock_id: stock.id) # checks that stock exists and creates new StockList record. This action is idempotent (will not double create)
        flash.now[:success] = t('controller.stock_search.actions.add_watched_stock.success')
      else
        puts 'Error in saving new watched stock: ' + stock.errors.messages.to_s
        flash.now[:alert] = t('controller.stock_search.actions.add_watched_stock.error.missing_data')
      end
    else
      puts 'Could not add watched stock because :to_watch param was missing.'
      flash.now[:alert] = t('controller.stock_search.actions.add_watched_stock.error.general')
    end
    render :search # return to search page after adding the stock
  end

end
