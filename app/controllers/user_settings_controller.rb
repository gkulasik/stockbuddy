class UserSettingsController < ApplicationController
  ##
  ### CALL BACKS
  ##

  before_action :set_user_settings, only: [:edit, :update, :resend_sms_verification_code]


  # Design note: UserSettings are automatically created when a user signs up - therefore there is no create method. Check User callbacks.
  # Users cannot destroy their user settings - they are automatically destroyed with the user account

  ##
  ### CRUD (edit and update only)
  ##

  # Get settings page (this is the show and edit page combined. All fields are editable immediately)
  # Build a new phone_info to allow the form to render a new set of fields that can be used to add a new phone.
  # Params
  # @settings - user_setting of the user currently logged in (auto set)
  def edit
    @settings.phone_infos.build
  end

  #TODO check that translations are working
  # Update settings
  # Updates the user settings including phone information (see UserSetting model)
  # Params
  # all the user setting parameters (strong parameters) - values to update the account to
  # @settings - user_settings to update
  def update
    if @settings.update(user_setting_params)
      flash[:success] = t('controller.user_settings.flash.update.success')
      redirect_to edit_user_setting_path(@settings)
    else
      flash[:alert] = t('controller.user_settings.flash.update.error')
      @settings.phone_infos.build # rebuild since the previous (from edit) was thrown out in model 'reject_if'
      render :edit
    end
  end

  ##
  ### CUSTOM
  ##

  ## SMS VERIFICATION

  ## Design Note: Since PhoneInfo has no controller most controller actions regarding PhoneInfo will be in this controller

  # Resend a sms verification code
  # Checks if the correct parameter is present and whether the signed in user owns the phone that would receive the verification text (don't want others trying to resend the codes)
  # Then sets the new sms verification code and resends
  # The method is idempotent and can be called again (if somehow accessible through a seperate session)
  # Params
  # phone - the id of the phone to resend the code too
  # @settings - to be able to verify the user owns this phone being reset
  def resend_sms_verification_code
    # Verifies the data is present and the current user has a phone with that id
    if !params[:phone].blank? && @settings.phone_infos.pluck(:id).include?(params[:phone].to_i)
      phone = @settings.phone_infos.find_by(id: params[:phone]) # know phone exists cause of above check
      if phone.needs_confirming # check if should resend the code or not (has it already been confirmed?)
        phone.resend_sms_code
        flash[:success] = t('controller.user_settings.action.resend_sms_verification_code.success')
      else
        flash[:info] = t('controller.user_settings.action.resend_sms_verification_code.already_verified')
      end
    else
      flash[:alert] = t('controller.user_settings.action.resend_sms_verification_code.error')
    end
    redirect_to edit_user_setting_path(@settings)
  end

  private

  # Sets the @settings variable using the currently logged in user (ignores the parameter passed)
  def set_user_settings
    # TODO Note: when adding in admin functionality will need to adjust this method (so that an admin can see the users settings)
    # Can be avoided probably with an out of the box admin interface
    @settings = UserSetting.includes(:phone_infos).find_by(user_id: current_user.id)
  end

  def user_setting_params
    # NOTE: add new params for the user settings here - including new attributes for phone_info
    params.require(:user_setting).permit(:alert_sms, :alert_email,
                                         phone_infos_attributes: [:id, :phone_number, :carrier, :primary, :user_setting_id, :_destroy, :code_to_verify])
  end

end
