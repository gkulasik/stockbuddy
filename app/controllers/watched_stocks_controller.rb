class WatchedStocksController < ApplicationController
  ##
  ### CALL BACKS
  ##

  before_action :set_watched_stock, only: [:destroy]

  ##
  ### CRUD (Only index and destroy)
  ##

  # GET /watched_stocks
  # User Watched stock page - where they see all their watched stocks
  # This method will get all the current users watched stocks and display them to them
  def index
    @watched_stocks = current_user.get_watched_stocks.page(params[:page])
  end

  def show
    # TODO temporary to introduce the show links for individual stocks - later will change to actually show the stock alone with details.
    @watched_stocks = current_user.get_watched_stocks.page(params[:page])
    render :index
  end


  # DELETE /watched_stocks/1
  # Destroy a watched stock
  # This method is not heavily used other than in tests
  # Possible use: remove watched stocks that are no longer watched (why keep updating it?)
  # Params
  # @watched_stock - uses id param to get the correct watched stock
  def destroy
    # TODO future feature - job to run nightly to check whether all watched stocks are at least watched by one person - prune any that are not? Need to consider the ramifications of this - may want to keep the stock data to run analysis on
    # Database constraint will prevent a watched stock from being deleted if stock_lists still reference it
    @watched_stock.destroy
    redirect_to watched_stocks_url, notice: t('controller.watched_stocks.flash.destroy.success')
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_watched_stock
      @watched_stock = WatchedStock.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def watched_stock_params
      # NOTE: add new attributes here to allow them through for creating and updating
      params.require(:watched_stock).permit(:name, :ticker, :current_price)
    end

  # Provides list of params to filter alerts by. Linked to StockAlert model scopes.
  def filtering_params(params)
    params.slice(:with_trigger_status)
  end
end
