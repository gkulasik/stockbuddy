require "administrate/base_dashboard"

class PhoneInfoDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
      user_setting: Field::BelongsTo,
      id: Field::Number,
      phone_number: Field::String,
      carrier: Field::String,
      created_at: Field::DateTime,
      updated_at: Field::DateTime,
      primary: Field::Boolean,
      verify_code: Field::String,
      confirmed: Field::Boolean,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
      :phone_number,
      :carrier,
      :primary,
      :confirmed,
      :verify_code
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
      :user_setting,
      :id,
      :phone_number,
      :carrier,
      :primary,
      :verify_code,
      :confirmed,
      :created_at,
      :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
      :phone_number,
      :carrier,
      :primary,
      :verify_code,
      :confirmed,
  ].freeze

  # Overwrite this method to customize how phone infos are displayed
  # across all pages of the admin dashboard.
  #
  def display_resource(phone_info)
    "#{phone_info.phone_number} (#{phone_info.carrier})"
  end
end
