require "administrate/base_dashboard"

class StockAlertDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    stock_list: Field::BelongsTo,
    id: Field::Number,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    triggered: Field::Boolean,
    reset_token: Field::String,
    value: Field::String,
    alert_name: Field::String,
    type: Field::Select.with_options(
        collection: StockAlert.subtypes.collect { |klass| klass.name}
    ),
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :type,
    :alert_name,
    :value,
    :triggered
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :stock_list,
    :id,
    :triggered,
    :reset_token,
    :value,
    :alert_name,
    :type,
    :created_at,
    :updated_at
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :stock_list,
    :triggered,
    :reset_token,
    :value,
    :alert_name,
    :type,
  ].freeze

  # Overwrite this method to customize how stock alerts are displayed
  # across all pages of the admin dashboard.
  #
  def display_resource(stock_alert)
    "#{stock_alert.alert_name || I18n.t('models.stock_alerts.name.single') + ' ' + stock_alert.id.to_s}"
  end
end
