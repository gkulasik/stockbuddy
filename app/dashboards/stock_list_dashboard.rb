require "administrate/base_dashboard"

class StockListDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
      user: Field::BelongsTo,
      watched_stock: Field::BelongsTo,
      stock_alerts: Field::HasMany,
      id: Field::Number,
      created_at: Field::DateTime,
      updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
      :id,
      :watched_stock,
      :stock_alerts,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
      :user,
      :watched_stock,
      :stock_alerts,
      :id,
      :created_at,
      :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
      :user,
      :watched_stock
  ].freeze

  # Overwrite this method to customize how stock lists are displayed
  # across all pages of the admin dashboard.
  def display_resource(stock_list)
    "Interest in #{stock_list.watched_stock.ticker}"
  end
end
