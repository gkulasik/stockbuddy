require "administrate/base_dashboard"

class WatchedStockDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  MONEY_OPTIONS = {decimals: 2, prefix: '$'}

  ATTRIBUTE_TYPES = {
      stock_lists: Field::HasMany,
      id: Field::Number,
      name: Field::String,
      ticker: Field::String,
      current_price: Field::Number.with_options(MONEY_OPTIONS),
      created_at: Field::DateTime,
      updated_at: Field::DateTime,
      last_price: Field::Number.with_options(MONEY_OPTIONS),
      change: Field::Number.with_options(decimals: 2),
      change_percentage: Field::Number.with_options(decimals: 2),
      volume: Field::Number,
      average_volume: Field::Number,
      open: Field::Number.with_options(MONEY_OPTIONS),
      high: Field::Number.with_options(MONEY_OPTIONS),
      low: Field::Number.with_options(MONEY_OPTIONS),
      close: Field::Number.with_options(MONEY_OPTIONS),
      prev_close: Field::Number.with_options(MONEY_OPTIONS),
      week_52_high: Field::Number.with_options(MONEY_OPTIONS),
      week_52_low: Field::Number.with_options(MONEY_OPTIONS),
      prev_week_52_high: Field::Number.with_options(MONEY_OPTIONS),
      prev_week_52_low: Field::Number.with_options(MONEY_OPTIONS),
      asset_type: Field::String,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
      :id,
      :name,
      :ticker,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
      :id,
      :name,
      :ticker,
      :current_price,
      :last_price,
      :change,
      :change_percentage,
      :volume,
      :average_volume,
      :open,
      :high,
      :low,
      :close,
      :prev_close,
      :week_52_high,
      :week_52_low,
      :prev_week_52_high,
      :prev_week_52_low,
      :asset_type,
      :created_at,
      :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
      :name,
      :ticker,
      :current_price,
      :last_price,
      :change,
      :change_percentage,
      :volume,
      :average_volume,
      :open,
      :high,
      :low,
      :close,
      :prev_close,
      :week_52_high,
      :week_52_low,
      :prev_week_52_high,
      :prev_week_52_low,
      :asset_type,
  ].freeze

  # Overwrite this method to customize how watched stocks are displayed
  # across all pages of the admin dashboard.
  #
  def display_resource(watched_stock)
    "#{watched_stock.name} (#{watched_stock.ticker})"
  end
end
