class StockAlertDecorator < Draper::Decorator
  delegate_all

  def alert_name
    object.alert_name || ''
  end

  def description
    watched_stock = object.get_watched_stock
    h.link_to(watched_stock.ticker, watched_stock) + Formatter.format_text(object.view_text, object.view_params)
  end

  def unlinked_description
    watched_stock = object.get_watched_stock
    watched_stock.ticker + Formatter.format_text(object.view_text, object.view_params)
  end

end
