class WatchedStockDecorator < Draper::Decorator
  delegate_all # provides access to 'object' or 'model' (same thing)
  # Note: object == WatchedStock

  # @return [String] formatted name
  def name
    h.truncate(object.name, length: 20)
  end

  # @return [String] formatted current price
  def current_price
    money(object.current_price)
  end

  # @return [String] formatted last price
  def last_price
    money(object.last_price)
  end

  # @return [String] formatted change
  def change
    money(object.change)
  end

  # @return [String] formatted open
  def open
    money(object.open)
  end

  # @return [String] formatted high
  def high
    money(object.high)
  end

  # @return [String] formatted low
  def low
    money(object.low)
  end

  # @return [String] formatted previous close
  def prev_close
    money(object.prev_close)
  end

  # @return [String] formatted close
  def close
    money(object.close)
  end

  # @return [String] formatted 52 week high
  def week_52_high
    money(object.week_52_high)
  end

  # @return [String] formatted 52 week low
  def week_52_low
    money(object.week_52_low)
  end

  # @return [String] formatted asset type
  def asset_type
    object.asset_type.capitalize
  end

  # @return [String] formatted previous 52 week high
  def prev_week_52_high
    money(object.prev_week_52_high)
  end

  # @return [String] formatted previous 52 week low
  def prev_week_52_low
    money(object.prev_week_52_low)
  end

  # @return [String] formatted change percentage
  def change_percentage
    percent(object.change_percentage)
  end

  # Custom as to_json does not work due to "SystemStackError: stack level too deep" error.
  # Note: Need to update this if will be extending/adding more methods
  # @param [Hash] options (not used currently)
  # @return [Hash] hash to format as JSON
  def as_json(options = {})
    {
        name: object.name,
        ticker: object.ticker,
        current_price: current_price,
        last_price: last_price,
        change: change,
        change_percentage: change_percentage,
        volume: object.volume,
        average_volume: object.average_volume,
        open: open,
        high: high,
        low: low,
        close: close,
        prev_close: prev_close,
        week_52_high: week_52_high,
        week_52_low: week_52_low,
        prev_week_52_high: prev_week_52_high,
        prev_week_52_low: prev_week_52_low,
        asset_type: asset_type
    }
  end

  private


  # @param [Object] value to format
  # @return [String] formatted value or empty string if nil
  def money(value)
    value.nil? ? empty : Formatter.as_money(value).format_value
  end

  # @param [Object] value to format
  # @return [String] formatted value or empty string if nil
  def percent(value)
    value.nil? ? empty : Formatter.as_percent(value).format_value
  end

  def empty
    h.t('views.watched_stocks.index.current_price.none')
  end

end