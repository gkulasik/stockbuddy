module ApplicationHelper
  include SMSEasyHelper # Include SMS easy helper for other helpers (to trickle down)

  # Mapper for general flash classes to our classes
  # This is helpful for Devise flashes (which are a bit different than ours)
  # @param [String] level SYMBOL for the type of flash
  # @return [String] the type of class to use
  def flash_class(level)
    # When adding new 'when' must update application_helper_test
    case level.to_sym
      when :notice then
        'alert-box success' # Devise
      when :success then
        'alert-box success'
      when :info then
        'alert-box info'
      when :error then
        'alert-box alert'
      when :alert then
        'alert-box alert'
      when :recaptcha_error then
        'alert-box alert'
      else
        raise 'Missing flash_class mapping for ' + level
    end
  end

  # Helper to format created_at time in a consistent fashion
  # @param [Object] object to format
  # @return [String] formatted created_at time
  def formatted_created_at(object)
    formatted_date_time(object, 'created_at')
  end

  # Helper to format an amount into a currency
  # @param [Object] amount (int, double, string)
  # @return [String] formatted to currency
  def formatted_currency(amount)
    number_to_currency(amount)
  end

  # Helper to format an amount into a percentage
  # @param [Object] amount (int, double, string)
  # @param [Hash] options extra options (like precision)
  def formatted_percentage(amount, options={})
    options[:precision] = options[:precision] || 2 # default precision
    number_to_percentage(amount, options)
  end

  private
  # Helper to perform formatting - will return empty string if object == nil
  # @param [Object] object to format
  # @param [String] field to format
  def formatted_date_time(object, field)
    if object != nil
      object.send(field).strftime('%B %-d, %Y')
    else
      ''
    end
  end
end
