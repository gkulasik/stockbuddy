module FormHelper

  # Helper to create uniform form fields with minimal markup
  # TODO refactor this into something a bit less restrictive (create a new method and for legacy use the new method in this method)
  # @param [Form] form ActiveRecord form object
  # @param [String] num_and_type_columns ex. small-7
  # @param [String] field_name name of the field
  # @param [String] field_type type of the field (ex. text_field, select_field)
  # @param [Hash] field_options (options that can be applied to a field)
  # @param [String] label_text the label text (if not using default)
  # @param [Boolean] required whether the field is required or not
  # @param [String] tool_tip if want a tool tip provide text
  # @param [String] tool_tip_location move the tool tip around (top, bottom, left, right)
  # @param [Object] custom_option special field for select fields
  # @param [Boolean] reverse should the label and input be reversed
  # @return [String] html ready string for a field
  def field_maker(form, num_and_type_columns, field_name, field_type, field_options = {}, label_text = '', required = false, tool_tip = '', tool_tip_location = 'tip-top', custom_option = nil, reverse = false)
    content_tag :div, class: 'row' do
      content_tag :div, class: "#{num_and_type_columns} columns field" do
        label = build_label(form, field_name, required, label_text)
        final_label = if !tool_tip.empty?
                        tool_tip_helper(tool_tip, label, tool_tip_location)
                      else
                        label
                      end
        final_input = build_input(form, field_type, field_name, field_options, custom_option)
        if reverse
          concat final_input
          concat final_label
        else
          concat final_label
          concat final_input
        end
      end
    end
  end

  # Helper to show errors for an object easily.
  # @param [Object] resource object that may have errors to show
  # @return [String] html to display the errors
  def form_error_helper(resource)
    return '' if resource.errors.empty? # guard
    messages = resource.errors.full_messages.map {|msg| content_tag(:li, msg)}.join.html_safe
    sentence = I18n.t('errors.messages.not_saved',
                      count: resource.errors.count,
                      resource: resource.class.model_name.human.downcase)
    html = f_row do
      concat f_columns([:medium_6, :end], id: "error_explain") {|columns|
        header = content_tag :div, class: 'alert-box alert' do
          concat content_tag :strong, sentence
        end
        errors = content_tag :div do
          concat content_tag(:ul, messages)
        end
        header + errors
      }
    end

    # html = <<-HTML
    # <div id="error_explanation">
    #   <h2>#{sentence}</h2>
    #   <ul>#{messages}</ul>
    # </div>
    # HTML

    html.html_safe
  end

  private
  # Helper to build a label for the field maker
  # @param [Form] form ActiveRecord Form object
  # @param [String] field_name name of the field
  # @param [Boolean] required if the field required or not
  # @param [String] label_text text for the label
  # @return [String] html for the label
  def build_label(form, field_name, required = false, label_text = '')
    label_text_correct = label_text.empty? ? field_name.to_s.humanize : label_text # determine which text to use
    label_to_show = required ? label_text_correct + '*' : label_text_correct # add required '*' if needed
    form.label field_name, label_to_show
  end

  # Helper to build the input for the field maker
  # @param [Form] form ActiveRecord Form object
  # @param [String] field_type type of the field
  # @param [String] field_name name of the field
  # @param [Hash] field_options extra options
  # @param [Object] custom_options for select tag only
  # @return [String] html for the input
  def build_input(form, field_type, field_name, field_options = {}, custom_options = nil)
    case field_type
      when 'text_field'
        form.send(field_type, field_name, field_options)
      when 'password_field'
        form.send(field_type, field_name, field_options)
      when 'select'
        form.send(field_type, field_name, custom_options, field_options)
      when 'check_box'
        form.send(field_type, field_name, field_options)
      when 'text_area'
        form.send(field_type, field_name, field_options)
      else
        puts 'field_type not supported, defaulting to text_field'
        form.send(field_type, field_name, field_options)
    end
  end
end