require 'active_support/inflector'
module FoundationHelper

  # Helper to convert code into HTML (without having to have so much HTML code)
  # Usage:
  #   <%= f_row do %>
  #       {your content here}
  # <% end %>
  # @param [String] klasses additional classes (css) for the row div
  # @param [Hash] options for the row div
  # @param [Block] block
  def f_row(klasses = [], options = {}, &block)
    options[:class] = "#{klasses.map { |k| k.to_s }.join(' ')} row"
    content_tag :div, options do
      concat capture(&block)
    end.html_safe
  end

# Helper to convert code into HTML (without having to have so much HTML code)
# Usage:
#   <%= f_columns([:small_6]) do %>
#       {your content here}
# <% end %>
# The above example can go inside of a f_row block
# @param [Array<String>] klasses classes for the columns. For classes use symbols: :small_6, :medium_12, :large_8, etc. (enter the class as a symbol)
# @param [Hash] options additional options for the row
# @param [Object] block
# @return [String] html for the columns div
  def f_columns(klasses = [], options={}, &block)
    raise 'Must provide at least once class' if klasses.empty?
    options[:class] = "#{klasses.map { |k| k.to_s.dasherize }.join(' ')} columns"
    content_tag :div, options do
      concat capture(&block)
    end.html_safe
  end

# Tool tip helper to get a tool tip on some content
# @param [String] tip_text text of the tip
# @param [String] tipped_content content to wrap in a tip
# @param [String] tip_location location of the tip
# @return [String] html for the tip and the tipped_content
  def tool_tip_helper(tip_text, tipped_content, tip_location = 'tip-top')
    content_tag :span, class: "has_tip #{tip_location}", title: tip_text, :'aria-haspopup' => true, :'data-tooltip' => '' do
      tipped_content
    end
  end

# Options expected as hash of content and href
# @param [String] unique_id id of the dropdown (so that JS knows which you are clicking)
# @param [String] button_text text of the button
# @param [Array[String]] options options for the dropdown (as a list of strings)
# @param [String] button_size size of the button
# @return [String] html for the button
  def dropdown_helper(unique_id, button_text, options, button_size = 'small')
    html = content_tag :button, href: '#', :'data-dropdown' => "drop-#{unique_id}", :'aria-controls' => "drop-#{unique_id}", :'aria-expanded' => false, class: "#{button_size} button dropdown" do
      button_text
    end
    html = html + '<br/>'.html_safe

    dropdown = content_tag :ul, id: "drop-#{unique_id}", :'data-dropdown-content' => '', class: 'f-dropdown', :'aria-hidden' => true do
      options.each do |opt|
        concat content_tag :li, opt
      end
    end
    (html+dropdown).html_safe
  end

# Helper to create icons without having to remember the syntax - and reduce the wordiness of the html in views
# Requires icons to be part of the gem package
# @param [String] icon_name name of the icon  ex. 'check', 'list', or see http://zurb.com/playground/foundation-icon-fonts-3
# @param [String] icon_size symbol size of the icon (:h1, :h2, :h3, :p, :inline, etc.)
# @return [String] html of the icon
  def icon_helper(icon_name, icon_size)
    base = "<i class='fi-#{icon_name}'></i>"
    case icon_size
      when :inline
        base
      else
        content_tag(icon_size, base.html_safe)
    end.html_safe
  end
end