module StockAlertsHelper
  # TODO test
  # @param [Object] alert_type StockAlert subclass object or Class
  # @param [Form] form ActiveRecord form
  # @return [View]  Renders the partial
    def render_alert_form(alert_type, form)
      render "/stock_alerts/alert_forms/#{alert_type.partial_name}", f: form
    end
end
