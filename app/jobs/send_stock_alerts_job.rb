require 'benchmark'
class SendStockAlertsJob < ActiveJob::Base
  include ActionView::Helpers::NumberHelper
  queue_as :alert_notify

  # Perform action for the job (called by ActiveJob)
  # Main job of perform action is to loop through all the StockLists and decide who to notify
  # @return [Nil] Job does not return anything
  def perform
    # TODO is there a way to create a check that the stock alert job was run at least X times in the last hour? - Do when the site is run more frequently
    # Maybe some sort of variable or DB entry - this way an alert can be fired if the stock alerter is down
    begin
      Benchmark.bm do |b| # benchmarking to see the time for this
        b.report('SendStockAlertsJob: ') {
          puts 'Running'
          # get batch of stock lists
          ## TODO clean up to use find_each - will simplify the loop a bit
          StockList.joins(:stock_alerts).includes(:watched_stock,
                                                  :user,
                                                  :stock_alerts).find_in_batches do |stock_list_w_alerts|
            stock_list_w_alerts.each do |list_item| # work on each stock list
              list_item.stock_alerts.each do |alert| # work on each alert and possibly notify
                if alert.should_alert? # this determines if the alert should trigger a notification
                  puts "Notifying user of stock alert: #{list_item.watched_stock.ticker}"
                  notify(list_item.user, list_item.watched_stock, alert)
                end
              end
            end
          end
        }
      end
    rescue StandardError => e
      on_failure(e)
    end
  end

  # Method to allow work to be done if a job fails for whatever reason
  # @param [Exception] exception Exception that was caught
  def on_failure(exception)
    puts exception
  end

  private

  # Notifies the user in the way they desire (email/sms)
  # @param [User] user to notify
  # @param [WatchedStock] stock associated with triggered alert
  # @param [StockAlert] alert triggered alert
  # @return [Nil] does not return anything
  def notify(user, stock, alert)
    settings = user.user_setting
    notified = false
    # shorten token to accommodate for SMS text limit (160 chars)
    reset_token = Security.generate_random_token(3)
    alert.reset_token = reset_token
    if settings.alert_sms && settings.has_phone?
      notify_sms(settings.get_primary_phone, stock, alert)
      notified = true
    end
    if settings.alert_email
      notify_email(user, stock, alert)
      notified = true
    end
    # Note that the alert has been triggered so to not get continuous notifications
    alert.update(triggered: notified, reset_token: reset_token)
  end

  # Helper used to notify via SMS
  # @param [PhoneInfo] phone_info
  # @param [WatchedStock] stock
  # @param [StockAlert] alert
  def notify_sms(phone_info, stock, alert)
    # get the sms carrier email via SMSEasy
    sms_email = phone_info.sms_address
    # send mailer - message crafted in the mailer
    result = if phone_info.can_send
               StockAlertMailer.notify_price_sms(sms_email, stock, alert).deliver_later
             else
               "Phone not verified - not sending. Phone: #{phone_info.inspect}, Stock: #{stock.inspect}, Alert: #{alert.inspect}"
             end
    logger.info(result)
  end

  # Helper used to notify via email
  # @param [User] user to notify via email
  # @param [WatchedStock] stock associated with the alert
  # @param [StockAlert] alert the triggered alert
  def notify_email(user, stock, alert)
    result = StockAlertMailer.notify_price_email(user, stock, alert).deliver_later
    logger.info(result)
  end


end
