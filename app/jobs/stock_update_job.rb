class StockUpdateJob < ActiveJob::Base
  queue_as :stock_update


  # Perform action called by ActiveJob to update the data on all the WatchedStocks in the system
  # @return [Nil] Job does not return anything
  def perform
    # TODO is there a way to create a check that the stock update job was run at least X times in the last hour?
    # Maybe some sort of variable or DB entry - this way an alert can be fired if the stock updater is down
    begin
      Benchmark.bm do |b| # benchmarking to see the time for this
        b.report('StockUpdateJob: ') {
          puts 'Running'
          if Delayed::Job.where(queue: 'stock_update').count <= 1
            group_count = 0
            WatchedStock.find_in_batches do |all_stocks| # 1000 WatchedStock
              stocks_in_groups = all_stocks.in_groups_of(STOCKS_PER_REQUEST) # split up the large 1000 into smaller groups for the API requests
              stocks_in_groups.each do |group|
                group_count = group_count + 1
                puts "Processing group: #{group_count}"
                result = StockDataInterface.get_quotes(group.compact.map { |ws| ws.ticker })
                result.each do |stock|
                  update_stock(stock)
                end
              end
            end
            puts "Total batches: #{group_count}"
            SendStockAlertsJob.perform_later # set the alerts check to be performed next
            StockUpdateJob.set(wait: 1.minutes).perform_later
          else
            puts 'Too many duplicate jobs in queue - skipping'
          end
        }
      end
    rescue StandardError => e
      on_failure(e)
    end

  end

  # Method to allow work to be done if a job fails for whatever reason
  # @param [Exception] exception Exception that was caught
  def on_failure(exception)
    puts(exception)
  end

  private

  # Performs the update on an individual Stock
  # @param [StockDataInterface] stock_to_update
  # @return [Nil] No return value
  def update_stock(stock_to_update)
    # find the stock match from DB
    db_stock = WatchedStock.find_by(ticker: stock_to_update.ticker)
    # Old db_stock call - probably not as efficient to search in the code when can search on an indexed field
    # db_stock = @all_stocks.select { |stock| stock.ticker == stock_to_update.ticker }.first

    # verify that the update bid > 0 (to avoid bad data/false triggers) and update
    if (stock_to_update.bid.nil? || stock_to_update.bid > 0) && !db_stock.nil? && db_stock.update_from_stock_data(stock_to_update)
      puts 'Updated ' + db_stock.name
    else
      puts 'Error could not update stock: ' + db_stock.errors.full_messages.to_s
    end
  end
end
