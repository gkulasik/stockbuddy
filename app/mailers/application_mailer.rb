class ApplicationMailer < ActionMailer::Base
  default from: ENV['MAILER_FROM']
  layout 'mailer'

  # Not sure what is happening here but it seems requiring both in this manner is needed as it doesn't seem to work 100% otherwise
  include StockAlertsHelper
  include ApplicationHelper
  helper :stock_alerts
  helper :application
end
