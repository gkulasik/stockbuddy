class SmsVerifyMailer < ApplicationMailer
  # NOTE: This mailer is not subject to the SMS restriction - this mailer WILL send sms.

  # Verify sms phone mailer - send a simple sms with verification code to be entered by the user
  # @param [String] phone_number_email phone as an email
  # @param [String] code verification code for the phone
  # @return [Nil] Nothing returned
  def verify(phone_number_email, code)
    message = t('mailers.sms_verify_mailer.verify.message', code: code.to_s)
    mail(to: phone_number_email, subject: t('mailers.sms_verify_mailer.verify.subject'), body: message)
  end
end
