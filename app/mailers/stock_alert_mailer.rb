class StockAlertMailer < ApplicationMailer

  # TODO verify translations are working
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.stock_alert_mailer.notify.subject
  #
  # @param [User] user
  # @param [WatchedStock] stock
  # @param [StockAlert] alert
  def notify_price_email(user, stock, alert)
    @user = user
    @stock = stock
    @alert = alert
    @intro = if @alert.alert_name.blank?
               t('mailers.stock_alert_mailer.notify_price_email.intro.no_alert_name', ticker: @stock.ticker)
             else
               t('mailers.stock_alert_mailer.notify_price_email.intro.has_alert_name', alert_name: @alert.alert_name)
             end
    subject = if @alert.alert_name.blank?
                t("mailers.stock_alert_mailer.notify_price_email.subject.#{@alert.partial_name}", ticker: @stock.ticker)
              else
                 t('mailers.stock_alert_mailer.notify_price_email.subject.has_alert_name', alert_name: @alert.alert_name)
              end
    @link = reset_trigger_stock_alert_url(id: @alert.id, reset_token: @alert.reset_token)

    mail(to: @user.email, subject: subject)

  end

  # NOTE: be sure that users want to/can receive stock alerts (check phone_info.can_send)
  # Ex. send_stock_alerts_job.rb - line 43
  # @param [String] phone_number_email
  # @param [WatchedStock] stock
  # @param [StockAlert] alert
  def notify_price_sms(phone_number_email, stock, alert)
    @stock = stock
    @alert = alert
    # craft message
    formatted_display = Formatter.format_text(t("mailers.stock_alert_mailer.notify_price_sms.message.#{@alert.class.partial_name}", ticker: @stock.ticker), @alert.short_view_params)
    # NOTE: When having the reset link in SMS the link it triggered by some other party - probably TMobile on send checks the links
    # NOTE 2: Figured it out, this has been happening for a while, the difference was that the URL was wrong in that it was root.com//rest/of/url, this was fixed in the application.yml file to be root.com/rest/of/url and the issue then presented itself. Choosing to remove the link regardless and eventually replace it with a system that can answer/reply to texts.
    # link = reset_trigger_stock_alert_url(@alert, reset_token: @alert.reset_token)
    # message_with_link = formatted_display + ' <br/> ' + t('mailers.stock_alert_mailer.notify_price_sms.message.reset_link', reset_link: link)

    # NOTE FUTURE REFERENCE: After much testing adding a subject like "Price alert: #{@stock.ticker} @ #{number_to_currency(@stock.current_price)}" seemed to be too long or complicated. SMS notifications were not coming in when it was that long. Changing to "#{@stock.ticker} alert" worked.
    mail(to: phone_number_email, subject: t('mailers.stock_alert_mailer.notify_price_sms.subject', ticker: @stock.ticker), body: formatted_display)
  end
end
