##
### Not used due to issues that companies may have with us using their logo's
##
class CompanyDataInterface
  # Courtesy of Clearbit.com

  # @type [String] name
  # @type [String] logo
  # @type [String] domain
  attr_accessor :name, :logo, :domain

  SIZE = '120' # default size of images (pixels)

  # @param [Hash] params to populate the CompanyDataInterface
  # @return [CompanyDataInterface]
  def initialize(params = {})
    params.each { |key, value| send "#{key}=", value }
  end

  # Gets a company data by making an API request for that data
  # @param [String] partial_name partial name (or whole name) of a company
  # @return [CompanyDataInterface] the company data
  def self.get_company_data(partial_name)
    uri = URI.parse("https://autocomplete.clearbit.com/v1/companies/suggest")
    uri.query = URI.encode_www_form('query' => partial_name)
    response = RestClient.get(uri.to_s)
    if response.code > 199 && response.code < 300 # Check code
      # Get the data
      data = JSON.parse(response.body)
      # Parse and convert to CompanyDataInterface
      results = data.map { |company|
        CompanyDataInterface.new(name: company['name'], logo: company['logo'], domain: company['domain'])
      }
      # Find the exact company (match by name) (there can be more than one with a partial name)
      final = results.select do |company|
        company.name == partial_name
      end
      # return either the company or a dummy empty value
      final.empty? ? default_company : final.first
    else
      default_company
    end
  end

  # Dummy company for the companies that are not found
  # @return [CompanyDataInterface] dummy default company (for no company/ not found)
  def self.default_company
    CompanyDataInterface.new(name: I18n.t('general.na'), logo: default_logo, domain: '')
  end

  # Helper for the default 'dummy' logo
  # @return [String] url of image for not found companies
  def self.default_logo
    "http://placehold.it/#{SIZE}?text=N/A"
  end

  # Gets a company's logo
  # @param [String] size size of the image to get in pixels (square)
  # NOTE: size is length of longest size
  # @return [String] URL of the company logo with the size parameter attached
  def get_company_logo_url(size = SIZE)
    if logo != self.class.default_logo
      logo+"?size=#{size}"
    else
      logo
    end
  end
end