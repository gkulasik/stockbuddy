module SubType extend ActiveSupport::Concern


  def update_with_type_validations(attributes = {})
    type = attributes[:type] || self.type # Determine type to evaluate as
    self.becomes(type.constantize).update(attributes)
  end

end