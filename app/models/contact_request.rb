class ContactRequest < MailForm::Base

  # Attributes for the contact request
  attribute :name
  validates_presence_of :name
  attribute :email
  validates_format_of :email, :with => /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i
  attribute :message
  validates_presence_of :message
  attribute :nickname,  :captcha  => true  # Honeypot trick for bots

  # Basic mailer info - from, to, subject
  def headers
    {
        :subject => "StockBuddy Contact Request for #{name} <#{email}>",
        :to => ENV['MAILER_TO'], # Must send from self to self because with Sparkpost the domain must be configured.
        :from => ENV['MAILER_FROM']
    }
  end

end
