class PhoneInfo < ActiveRecord::Base
  ## Relationships
  belongs_to :user_setting, inverse_of: :phone_infos

  ## Attributes
  attr_accessor :code_to_verify

  ## Validations
  validates :phone_number, format: {with: /\A[23456789]\d{9}\Z/,
                                    message: I18n.t('models.phone_infos.validations.format')}
  validates :phone_number, uniqueness: true
  validates :user_setting_id, :carrier, presence: true
  #validate :verify_sms
  # NOTE: if new validations are not working in this model then move them to the user_settings model (verify_phones). Not ideal but there is no way around it for now.

  ## Scopes
  default_scope { order('created_at DESC') } # keep phone_infos in the same order between updates


  ## Callbacks
  before_create :generate_sms_code # often followed by send_verification_sms
  before_validation :check_if_number_changed # prevents number changes after validation
  after_create :send_verification_sms

  ## Validation methods

  # TODO this is a validation but not referenced here - it is in user settings. This is because it is not called correctly within this model due to something with nested relationships.
  # Verify if the verification code entered is correct. Marks the phone as confirmed if they match.
  # @return [Boolean] true or false whether phone is confirmed correctly
  def verify_sms
    should_look_at = !code_to_verify.blank? && needs_confirming
    codes_match = code_to_verify == verify_code
    if should_look_at
      if codes_match
        # mark confirmed
        self.confirmed = true
        true
      else
        false
      end
    else
      true
    end
  end

  ## Regular methods

  # Can this phone be sent alerts?
  # @return [Boolean] true or false whether the phone_info is confirmed (SMS can be sent to it)
  def can_send
    confirmed
  end

  # Does this phone need confirming?
  # @return [Boolean] true or false whether the phone_info needs confirming
  def needs_confirming
    !phone_number.blank? && !confirmed
  end

  # What is the sms email formatted address of this phone?
  # @return [String] phone number converted to an email via SMSEasy (ex. 12323434556@tmo.net)
  def sms_address
    SMSEasy::Client.sms_address(phone_number, carrier)
  end

  # Resend the SMS code (this also means generating a new sms code for the phone)
  # @return [Nil] though the method may not return nil it is supposed to be a void method.
  def resend_sms_code
    generate_sms_code
    save! # save the new SMS code
    send_verification_sms
  end


  private

  ## Callback methods

  # Used to verify if a phone number changed - used to prevent someone from confirming and then changing to another phone number.
  # @return [Nil] checks whether the phone number changed - if it did then it un-confirms the phone_info
  def check_if_number_changed
    if self.phone_number_changed? && self.confirmed
      self.confirmed = false
    end
  end

  # Generates an sms code for the phone and assigns it to the phone's verify_code
  # @return [Nil] Generates a new random token for the phone_info
  def generate_sms_code
    token = Security.generate_random_token(3)
    self.verify_code = token
  end

  # Sends the SMS verification SMS/Email to the phone
  # @return [Nil] sends the verification SMS email
  def send_verification_sms
    SmsVerifyMailer.verify(sms_address, verify_code).deliver_later
  end

end
