class StockAlert < ActiveRecord::Base
  include SubType

  ## Relationships
  belongs_to :stock_list

  ## Validations
  validates :stock_list_id, :type, presence: true
  validate :default_value_validation

  ## Scopes
  default_scope { order(:id) }
  scope :with_trigger_status, -> (status) { where(triggered: status) }
  scope :alert_for, -> (stock_list_id) {where(stock_list_id: stock_list_id)}

  ## Pagination for StockAlerts page
  paginates_per 30

  ## Validation methods

  # Validation that if value is required/present that it must be positive
  # @return [Nil] Checks that value is positive - adds an error if not
  def default_value_validation
    if (!value.blank? && BigDecimal(value) < BigDecimal(0)) || (Float(value).nil? rescue true)
      errors.add(:value, I18n.t('models.stock_alerts.validations.positive_value'))
    end
  end

  # Get the StockAlert's WatchedStock (the stock that alerts are triggered on)
  # @return [WatchedStock] the associated watched stock for this alert
  def get_watched_stock
    self.stock_list.watched_stock
  end

  # Master should alert method - checks if the WatchedStock has the proper info for calculation and if the subclass has been triggered and if the triggered flag has been already set before.
  # @return [Boolean] t/f whether the owner of this alert should be alerted
  def should_alert?
    stock = self.get_watched_stock
    self.validate_watched_stock(stock) && self.should_alert(stock) && !self.triggered
  end

  def get_safe_attribute(attr = nil)
    self.send(attr) rescue nil
  end

  # Gets all subtypes without requiring there to be a database entry like descendants or subclasses
  # @return [Array<Class>] Array of the subtypes of StockAlert
  # NOTE: must add any new subtypes here
  def self.subtypes
    [AboveAverageVolume, New52WeekHigh, New52WeekLow, PriceGreaterThan, PriceLessThan, PriceDecreaseAmount, PriceDecreasePercent, PriceIncreaseAmount, PriceIncreasePercent, PriceRange]
  end

  # @return [StockAlert] the stock alert AS its type field
  def as_self_type
    self.becomes(self.type.constantize)
  end

  def preprocess(params = nil)
    nil
    # Do nothing - used for StockAlert types that need to do any preprocessing before validation in controller
  end

  ## TODO Move to a decorator - debating if should - seems like a lot of work for minimal benefit (3/12/17)
  ## Subclass methods - implemented by all subclasses
  # Only documented here for brevity

  # Value to show users when creating/editing stock alerts
  # @return [String] the select box value to show when creating/editing stock alerts
  def self.select_text
    raise 'Not implemented'
  end

  # Links to the en.yml file for translations
  # @return [String] the universal name of a partial (used in mailers, sms, forms)
  def self.partial_name
    raise 'Not implemented'
  end

  # Helper to get to self.partial (avoid errors because forgot to call on the class rather than the object)
  # @return [String] the universal name of a partial (used in mailers, sms, forms)
  def partial_name
    self.class.partial_name
  end

  # Text to display when viewing already created stock alerts (alert when... {what happens})
  # Populated by view_params and short_view_params
  # @return [String] raw un-formatted text
  def view_text
    raise 'Not implemented'
  end

  # Populates view_text
  # Array of formatters for various values to insert params into the view_text
  # @return [Array<Formatter>] the formatters to format view_text
  def view_params
    raise 'Not implemented'
  end

  # Populates view_text
  # Array of formatters for various values to insert params into the view_text
  # @return [Array<Formatter>] the formatters to format view_text
  def short_view_params
    raise 'Not implemented'
  end

  # These methods should only be called within the context of StockAlert or subclasses
  protected

  # Should the alert be triggered according to this stock alert subclass
  # This method assumes the stock is present and that the WatchedStock has been validated
  # @param [WatchedStock] stock the stock to validate
  # @return [Boolean] whether or not to alert - at a subclass level
  def should_alert(stock)
    raise 'Not implemented'
  end

  # Check if the watched stock has the required data to perform the calculation (as values may be nil)
  # @param [WatchedStock] stock the stock to validate
  # @return [Boolean] true if the stock has present all the attributes that are necessary to perform the alert calculation
  def validate_watched_stock(stock)
    raise 'Not implemented'
  end

end
