class AboveAverageVolume < StockAlert
  # TODO all StockAlert types - check that translations are working
  validates :value, presence: true

  def should_alert(stock)
    percent = self.class.calc_change(stock)
    alert_threshold = BigDecimal(value)
    # compares if the % change in volume is greater than the value the user wishes to be alerted at.
    percent >= alert_threshold
  end

  def self.select_text
    I18n.t('models.stock_alerts.subtypes.above_average_volume.select_text')
  end

  def self.partial_name
    'above_average_volume'
  end

  def view_text
    I18n.t('models.stock_alerts.subtypes.above_average_volume.view_text', formatter: Formatter.percent)
  end

  def view_params
    [Formatter.as_percent(value)]
  end

  def short_view_params
    stock = get_watched_stock
    actual_percent = self.class.calc_change(stock)
    [Formatter.as_percent(actual_percent), Formatter.as_percent(value)]
  end

  # @param [WatchedStock] stock
  # @return [BigDecimal] percentage (as a whole number (ex. 32 not .32))
  def self.calc_change(stock)
    if validate_watched_stock(stock)
      current = BigDecimal(stock.volume)
      average = BigDecimal(stock.average_volume)
      diff = current - average
      (diff / average)*100 # to get as whole number
    else
      0
    end
  end

  def validate_watched_stock(stock)
    !stock.nil? && !stock.volume.nil? && !stock.average_volume.nil?
  end
end