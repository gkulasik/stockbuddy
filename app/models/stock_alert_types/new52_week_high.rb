class New52WeekHigh < StockAlert

  def should_alert(stock)
    prev_52_high = stock.prev_week_52_high
    current_52_high= stock.week_52_high
    day_high = stock.high
    # Validate that there is a new high and the day_high agrees with that
    prev_52_high != current_52_high && day_high >= current_52_high
  end

  # Overwrite since this class does not care about a value
  def default_value_validation
    # Do nothing (not validating value)
  end

  def self.select_text
    I18n.t('models.stock_alerts.subtypes.new52_week_high.select_text')
  end

  def self.partial_name
    # TODO should make these strings into symbols?
    'new_52_week_high'
  end

  def view_text
    I18n.t('models.stock_alerts.subtypes.new52_week_high.view_text')
  end

  def view_params
    []
  end

  def short_view_params
    stock = get_watched_stock
    [Formatter.as_money(stock.week_52_high)]
  end

  def validate_watched_stock(stock)
    !stock.nil? && !stock.high.nil? && !stock.week_52_high.nil? && !stock.prev_week_52_high.nil?
  end
end