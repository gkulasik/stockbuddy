class New52WeekLow < StockAlert

  def should_alert(stock)
    prev_52_low = stock.prev_week_52_low
    current_52_low = stock.week_52_low
    day_low = stock.low
    # Validate that there is a new low and the day_low agrees with that
    prev_52_low != current_52_low && day_low <= current_52_low
  end

  def self.select_text
    I18n.t('models.stock_alerts.subtypes.new52_week_low.select_text')
  end

  def self.partial_name
    'new_52_week_low'
  end

  def view_text
    I18n.t('models.stock_alerts.subtypes.new52_week_low.view_text')
  end

  def view_params
    []
  end

  def short_view_params
    stock = get_watched_stock
    [Formatter.as_money(stock.week_52_low)]
  end

  def validate_watched_stock(stock)
    !stock.nil? && !stock.low.nil? && !stock.week_52_low.nil? && !stock.prev_week_52_low.nil?
  end
end