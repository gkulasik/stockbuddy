class PriceDecreaseAmount < StockAlert
  validates :value, presence: true

  def should_alert(stock)
      threshold = BigDecimal(value) * BigDecimal(-1)
      # Stock.change is a whole number while value is always positive - so need to make value negative to compare effectively
      threshold >= stock.change
  end

  def self.select_text
    I18n.t('models.stock_alerts.subtypes.price_decrease_amount.select_text')
  end

  def self.partial_name
    'price_decrease_amount'
  end

  def view_text
    I18n.t('models.stock_alerts.subtypes.price_decrease_amount.view_text', formatter: Formatter.money)
  end

  def view_params
    [Formatter.as_money(value)]
  end

  def short_view_params
    stock = get_watched_stock
    [Formatter.as_money(stock.change), Formatter.as_money(value)]
  end

  def validate_watched_stock(stock)
    !stock.nil? && !stock.change.nil?
  end
end