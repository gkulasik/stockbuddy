class PriceDecreasePercent < StockAlert
  validates :value, presence: true

  def should_alert(stock)
    threshold = BigDecimal(value) * BigDecimal(-1)
    # Stock.change_percentage is a whole number while value is always positive - so need to make value negative to compare effectively
    threshold >= stock.change_percentage
  end

  def self.select_text
    I18n.t('models.stock_alerts.subtypes.price_decrease_percent.select_text')
  end

  def self.partial_name
    'price_decrease_percent'
  end

  def view_text
    I18n.t('models.stock_alerts.subtypes.price_decrease_percent.view_text', formatter: Formatter.percent)
  end

  def view_params
    [Formatter.as_percent(value)]
  end

  def short_view_params
    stock = get_watched_stock
    [Formatter.as_percent(stock.change_percentage), Formatter.as_percent(value)]
  end


  def validate_watched_stock(stock)
    !stock.nil? && !stock.change_percentage.nil?
  end
end