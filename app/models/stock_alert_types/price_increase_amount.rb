class PriceIncreaseAmount < StockAlert
  validates :value, presence: true

  def should_alert(stock)
    threshold = BigDecimal(value)
    # Compare both as positive values
    threshold <= stock.change
  end

  def self.select_text
    I18n.t('models.stock_alerts.subtypes.price_increase_amount.select_text')
  end

  def self.partial_name
    'price_increase_amount'
  end

  def view_text
    I18n.t('models.stock_alerts.subtypes.price_increase_amount.view_text', formatter: Formatter.money)
  end

  def view_params
    [Formatter.as_money(value)]
  end

  def short_view_params
    stock = get_watched_stock
    [Formatter.as_money(stock.change), Formatter.as_money(value)]
  end

  def validate_watched_stock(stock)
    !stock.nil? && !stock.change.nil?
  end
end