class PriceLessThan < StockAlert
  validates :value, presence: true

  def should_alert(stock)
    threshold = BigDecimal(value)
    # Is the current price < the value?
    stock.current_price < threshold
  end


  def self.select_text
    I18n.t('models.stock_alerts.subtypes.price_less_than.select_text')
  end

  def self.partial_name
    'price_less_than'
  end

  def view_text
    I18n.t('models.stock_alerts.subtypes.price_less_than.view_text', formatter: Formatter.money)
  end

  def view_params
    [Formatter.as_money(value)]
  end

  def short_view_params
    stock = get_watched_stock
    [Formatter.as_money(value), Formatter.as_money(stock.current_price)]
  end

  def validate_watched_stock(stock)
    !stock.nil? && !stock.current_price.nil?
  end
end