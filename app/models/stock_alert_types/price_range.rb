class PriceRange < StockAlert
  validates :value, presence: true
  validate :parseable
  validate :positive_values
  validate :correct_order

  attr_accessor :min, :max

  SPLITTER = '-' # custom splitter that takes text like 1-2 as a range from 1 to 2 (stores in min and max)

  after_initialize do |alert|
    parse unless value.nil?
  end

  def preprocess(params = nil)
    params[:stock_alert][:value] = params[:min] + '-' + params[:max]
  end

  def parse
    if parseable && correct_order
      values = value.split(SPLITTER)
      self.min = values[0]
      self.max = values[1]
      true
    else
      false
    end
  end

  def should_alert(stock)
    # Is the current price between the min and max?
    stock.current_price > BigDecimal(min) && stock.current_price < BigDecimal(max)
  end

  def self.select_text
    I18n.t('models.stock_alerts.subtypes.price_range.select_text')
  end

  def self.partial_name
    'price_range'
  end

  def view_text
    I18n.t('models.stock_alerts.subtypes.price_range.view_text', min: Formatter.money, max: Formatter.money)
  end

  def view_params
    [Formatter.as_money(min), Formatter.as_money(max)]
  end

  def short_view_params
    stock = get_watched_stock
    [Formatter.as_money(min), Formatter.as_money(max), Formatter.as_money(stock.current_price)]
  end

  def default_value_validation
    true # do nothing handled in validation here.
  end

  def parseable
    errorCount = errors.size
    errors.add(:base, I18n.t('models.stock_alerts.subtypes.price_range.validations.all_values_present.error')) unless all_values_present?
    errors.add(:base, I18n.t('models.stock_alerts.subtypes.price_range.validations.decimal_values.error')) unless decimal_values?
    errors.size == errorCount
  end

  def positive_values
    errorCount = errors.size
    errors.add(:base, I18n.t('models.stock_alerts.subtypes.price_range.validations.values_positive.error')) unless values_positive?
    errors.size == errorCount
  end

  def correct_order
    errorCount = errors.size
    errors.add(:base, I18n.t('models.stock_alerts.subtypes.price_range.validations.min_to_max_order.error')) unless min_to_max_order?
    errors.size == errorCount
  end

  def validate_watched_stock(stock)
    !stock.nil? && !stock.current_price.nil?
  end

  private

  def all_values_present?
    values = value.split(SPLITTER)
    values.length == 2 # range only has two values
  end

  def decimal_values?
    values = value.split(SPLITTER)
    return false if values.empty?
    values.all? { |val| (!Float(val).nil? rescue false)}
  end

  def min_to_max_order?
    values = value.split(SPLITTER).map {|val| val.to_f} # "11" < "3" so need to convert to float
    sortedValues =  values.sort
    values == sortedValues
  end

  def values_positive?
    values = value.split(SPLITTER)
    values.all? { |val| val.to_d >= 0 }
  end
end