require 'tradier'
class StockDataInterface
  attr_accessor :name, # full friendly name of the asset
                :ticker, # symbol that represents the asset
                :bid, # ask price for the asset
                :last_price, # the last price traded at (should match bid)
                :change, # absolute amount price has changed today
                :change_percentage, # amount price has changed today as a percentage
                :volume, # current # of asset traded today
                :average_volume, # average # of asset traded in a day
                :open, # open for the day (what price started at)
                :high, # high for the day (highest price reached so far today)
                :low, # low for the day (lowest price reached so far today)
                :close, # close for the day (price the asset ended at end of trading day)
                :prev_close, # previous close (yesterday)
                :week_52_high, # high of the last year (moving)
                :week_52_low, # low of the last year (moving)
                :asset_type # What kind of asset is it? ex. stock
  # add more attributes if expanding functionality, make sure to update converter too

  # init (new)
  # @param [Hash] params to populate the StockDataInterface
  # @return [StockDataInterface]
  def initialize params = {}
    params.each { |key, value| send "#{key}=", value }
  end

  # Searches for stocks by free text
  # Returns empty list if nothing found or error
  # Searches both name and ticker
  # @param [String] query what to search for.
  # @return [Array<Hash>] Each hash has basic descriptions of the company
  def self.search(query)
    search = begin
      client.search(query) # Look up by name
    rescue StandardError => error
      puts error.inspect
      # TODO use error.class.superclass.inspect == 'Tradier::Error::ServerError' to check if the service is down - maybe return an object that rather than just a list
      [] # return empty list on error (not found)
    end
    lookup = begin
      client.lookup(query) # Look up by ticker
    rescue StandardError => error
      puts error.inspect
      []
    end
    (search + lookup).map { |s| {name: s.description, symbol: s.symbol} }
  end

  # Retrieves quotes for the tickers searched
  # @param [Array<String>] tickers tickers to look up info on
  # @param [Object] additional_params additional parameters not yet used
  # @return [Array<StockDataInterface>]
  def self.get_quotes(tickers, *additional_params)
    begin
      quotePre = client.quotes(tickers) # search quotes
      convert_api_stock_result_to_stock_data(quotePre) # convert API data to StockDataInterface data
    rescue StandardError => error
      puts 'Rescued error: Could not reach Tradier. Returning empty list. ' + error.message
      []
    end
  end

  # Honestly not sure if this is needed but should be kept up to date with the attr_accessor list above
  # @return [Array<String>] array of the field names of StockDataInterface
  def self.default_params
    [:name, :ticker, :bid, :last_price, :change, :change_percentage, :volume, :average_volume, :open, :high, :low, :close, :prev_close, :week_52_high, :week_52_low, :asset_type]
  end

  private

  # TODO test?
  # Map Tradier object to StockBuddy StockDataInterface object
  # @param [Array<Tradier::Quote>] output data from search
  # @return [Arrray<StockDataInterface>] converted data
  def self.convert_api_stock_result_to_stock_data(output)
    output.map { |s| StockDataInterface.new(name: s.attrs[:description],
                                            ticker: s.attrs[:symbol],
                                            bid: s.attrs[:bid],
                                            last_price: s.attrs[:last],
                                            change: s.attrs[:change],
                                            change_percentage: s.attrs[:change_percentage],
                                            volume: s.attrs[:volume],
                                            average_volume: s.attrs[:average_volume],
                                            open: s.attrs[:open],
                                            high: s.attrs[:high],
                                            low: s.attrs[:low],
                                            close: s.attrs[:close],
                                            prev_close: s.attrs[:prevclose],
                                            week_52_high: s.attrs[:week_52_high],
                                            week_52_low: s.attrs[:week_52_low],
                                            asset_type: s.attrs[:type]) }
  end

  # Get API client
  # @return [Tradier::Client] for making API calls
  def self.client
    Tradier::Client.new(access_token: ENV['TRADIER_API_KEY'], endpoint: ENV['TRADIER_ENDPOINT'])
  end
end