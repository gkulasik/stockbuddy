class StockList < ActiveRecord::Base
  ## Purpose
  # To map a user to a watched stock (this is essentially a record of the stocks that a user has watched)

  ## Relationships
  belongs_to :user
  belongs_to :watched_stock
  has_many :stock_alerts, dependent: :destroy

  ## Validations
  validates :user_id, :watched_stock_id, presence: true
  validates_associated :stock_alerts
  validate :no_duplicate_stock_lists

  ## Pagination for WatchedStocks page
  paginates_per 30

  # TODO add validation at application level to ensure uniqueness of user_id and watched_stock_id together (need to verify that this works)
  def no_duplicate_stock_lists
    exists = StockList.where(user_id: user_id, watched_stock_id: watched_stock_id).first
    if !exists.nil? && new_record?
      errors.add(:base, I18n.t('models.stock_lists.validations.no_duplicates'))
    end
  end
end
