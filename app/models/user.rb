class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable

  ## Validations
  validates :first_name, :last_name, presence: true
  validates_associated :stock_lists, :user_setting

  ## Devise
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable

  ## Relationships
  has_many :stock_lists, dependent: :destroy
  has_one :user_setting, dependent: :destroy

  ## Callbacks
  after_create :attach_settings

  # @return [String] users 'full name'
  def full_name
    self.first_name + ' ' + self.last_name
  end

  # Get all user's stock alerts
  # @return [Array<StockAlert>]
  def get_stock_alerts
    StockAlert.joins(:stock_list).where('stock_lists.user_id = ?', self.id)
  end

  # Get all of a user's watched stocks (as stocklists but they are preloaded)
  # @return [Array<StockList>]
  def get_watched_stocks
    self.stock_lists.includes(:watched_stock, :stock_alerts)
  end

  # Customization on how to send the Devise mail - now or later
  # @return [Nil] Mailer for Devise
  # TODO test - was there a reason for this to be deliver_now?
  def send_devise_notification(notification, *args)
    devise_mailer.send(notification, self, *args).deliver_later
  end

  private

  # Ensure every user has a user_settings (default email to true, sms to false)
  # @return [Boolean] t/f if the record saved
  def attach_settings
    setting = self.build_user_setting(alert_sms: false, alert_email: true)
    setting.save
  end

end
