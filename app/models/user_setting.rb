class UserSetting < ActiveRecord::Base
  ## Relationships

  has_many :phone_infos, dependent: :destroy, inverse_of: :user_setting
  belongs_to :user

  accepts_nested_attributes_for :phone_infos, allow_destroy: true, reject_if: lambda { |attributes| attributes['phone_number'].blank? && attributes['carrier'].blank? }

  ## Validations

  validates :user_id, presence: true, uniqueness: true # user must have settings and only one settings
  validate :if_has_phone_has_primary
  validate :if_alert_sms_check_has_phone
  validate :verify_phones # PhoneInfos validation

  ## Callbacks
  after_save :if_no_primaries_disable_sms

  ## Scopes
  default_scope { includes(:phone_infos) }


  ## Model methods

  # Does the user have a primary phone to send to?
  # @return [Boolean] t/f if a user has a primary phone
  def has_phone?
    !self.phone_infos.where(primary: true).empty?
  end

  # Retrieve the primary phone else nil
  # @return [PhoneInfo] gets the users primary phone
  def get_primary_phone
    results = self.phone_infos.where(primary: true)
    if results.empty?
      nil
    else
      results.first
    end
  end

  ## Validation methods

  # Will allow multiple primaries UNTIL alert SMS is checked - then will block and not allow save until there is only 1 primary
  # @return [Nil] validation on only one primary phone
  def if_has_phone_has_primary
    if !self.phone_infos.empty? && alert_sms
      primaries = phone_infos.select { |phone| phone.primary }
      if primaries.empty? || primaries.length > 1
        errors.add(:base, I18n.t('models.user_settings.validations.one_primary'))
      end
    end
  end

  # Validation that if SMS alerts == true there is a phone to send them to
  # @return [Nil]
  def if_alert_sms_check_has_phone
    if self.alert_sms && self.phone_infos.empty?
      errors.add(:base, I18n.t('models.user_settings.validations.has_phone'))
    end
  end


  # NOTE: Used to verify the fields of a phone that are not triggered via PhoneInfo model validations
  # For some reason nested model is not being FULLY validated (validate :method does not even fire for updates)
  # For now putting in the affected validations into the verify_phones method and calling the validation methods here for each
  # Second note: Do not validate in the PhoneInfo model and here - only validate here.
  # @return [Nil] Validations on PhoneInfos (check the individual methods in PhoneInfo for documentation)
  def verify_phones
    phone_infos.each do |pi|
      unless pi.verify_sms # Verify sms codes are valid
        # use schema of :'phone_infos.{attribute}'
        errors.add(:'phone_infos.phone_number', I18n.t('models.phone_infos.validations.sms_code_invalid'))
      end
      # Add new validations that are not firing properly below here (use same style as above)
    end
  end

  ## Callback methods

  # After save call back that checks if the user settings have any primary phones - the system cannot have alert sms set to true but no primary phones - unfortunately we do not get the _delete flag in the model. So we will check after the save.
  # @return [Boolean] t/f if saved successfully
  def if_no_primaries_disable_sms
    if alert_sms && get_primary_phone.nil?
      self.alert_sms = false
      save! # resave to commit the changes
    end
  end
end
