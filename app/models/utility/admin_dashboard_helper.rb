class AdminDashboardHelper
  ##
  ### SIDEBAR
  ##

  # List of items to hide from the sidebar
  HIDDEN_RESOURCES = [:price_ranges, :price_less_thans, :price_increase_percents, :price_increase_amounts, :price_greater_thans, :price_decrease_percents, :price_decrease_amounts, :new52_week_lows, :new52_week_highs, :above_average_volumes, :user_settings, :stock_lists, :phone_infos, :stock_alerts]

  # Takes the list of resources and returns a list without the hidden resources
  def self.processSideBarResources(resources)
    resources.reject { |i|
      i.resource.to_sym.in?(HIDDEN_RESOURCES)
    }
  end
end