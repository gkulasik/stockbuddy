# Utility class used to format raw data values
class Formatter
  include ActionView::Helpers::NumberHelper
  include ActionView::Helpers::UrlHelper
  include ApplicationHelper

  attr_accessor :value, :target, :format_method

  # Formatter denotations
  # Add new formatter types here as they are needed (make sure to integrate in the rest of the class too)
  PERCENT = '{%}'
  MONEY = '{$}'
  LINK = '{link}'

  # Init Formatter object
  # @param [String] value (can also be others but usually will be a string)
  # @param [String] target
  # @param [String] format_method
  # @return [Formatter]
  def initialize(value, format_method, target = nil)
    @value = value
    @format_method = format_method
    @target = target || format_method # if want to replace base formatter text ({%} else can specify more specific text)
  end

  # Init Percent Formatter
  # @param [String] value
  # @param [String] target
  # @return [Formatter]
  def self.as_percent(value, target = nil)
    Formatter.new(value, percent, target)
  end

  # Init Money Formatter
  # @param [String] value
  # @param [String] target
  # @return [Formatter]
  def self.as_money(value, target = nil)
    Formatter.new(value, money, target)
  end

  # Formats the value the Formatter was initialized with into the format that was specified
  # @return [String]
  def format_value
    case format_method
      when MONEY
        formatted_currency(BigDecimal(value, 2))
      when PERCENT
        formatted_percentage(BigDecimal(value, 2))
      else
        raise 'Format mapper missing'
    end

  end

  # Get percent formatter denotation (easier to say Formatter.percent than Formatter::PERCENT)
  # @return [String]
  def self.percent
    PERCENT
  end

  # Get money formatter denotation (easier to say Formatter.money than Formatter::MONEY)
  # @return [String]
  def self.money
    MONEY
  end

  # Helper to format text using a list of Formatters
  # This will allow the model to specify how it should represent itself and then provide where the values should be replaced.
  # Note: Order matters (notice the sub!)
  # @param [String] display_text ex. "test {%} test {$}" (text to format)
  # @param [Array<Formatter>] formatters (array of Formatters to format text)
  # @return [String] Formatted text
  def self.format_text(display_text, formatters = [])
    formatters.each do |f|
      formatted = f.format_value
      display_text.sub!(f.target, formatted) # replace first matching target
    end
    display_text
  end
end