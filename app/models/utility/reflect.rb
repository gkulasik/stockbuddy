class Reflect

  # Get a value from an object's field
  # Restrict to public methods for now
  # @param [Object] obj the object to get information about
  # @param [Symbol] field the field to get
  # @return [Object] the fields value
  def self.get(obj, field)
    unless field.nil?
      obj.public_send(field)
    else
      nil
    end
  end

  # Set a value on an object's field
  # @param [Object] obj object to mutate
  # @param [Symbol] field field to set
  # @param [Object] value value to set the field too
  # @return [Object] the value
  def self.set(obj, field, value)
    unless field.nil?
      field_inst = "@#{field}".to_sym
      obj.instance_variable_set(field_inst, value)
    else
      nil
    end
  end
end