class Security
  # The argument n specifies the length of the random length.
  # The length of the result string is twice of n.
  # @param [Int] n length of the token (actual length is 2x n)
  # @return [String]
    def self.generate_random_token(n = 10)
      SecureRandom.hex(n)
    end
end