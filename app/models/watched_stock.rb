class WatchedStock < ActiveRecord::Base

  ## Relationships
  has_many :stock_lists

  ## Validations
  validates :name, :ticker, presence: true, uniqueness: true # NOTE: only ticker uniqueness is DB enforced (and indexed)

  # Other fields are not currently validated because they may not be present for all stocks.

  # TODO after adding validations - test
  validates_associated :stock_lists

  ## Scopes
  # TODO test
  default_scope { order(:ticker) }

  # TODO test
  # Url to the watched stock image (company logo usually)
  # @return [String] url
  def image_url
    if self.blank?
      CompanyDataInterface.default_company.get_company_logo_url
    else
      self.image_url
    end
  end

  # TODO test
  # Sets attributes to use when creating a new WatchedStock for the first time
  # NOTE: Only use this to initiate fields before a create (DOES NOT RETURN A USABLE OBJECT)
  # Example use: StockSearchController.add_watched_stock
  # @param [StockDataInterface] data the data to convert into a partial WatchedStock
  # @return [WatchedStock::Relation]
  def self.init_from_stock_data(data)
    WatchedStock.create_with(map_stock_data(data))
  end

  # TODO test
  # Method to update attributes of existing stock
  # @param [StockDataInterface] new
  # @return [boolean] if the update succeeded
  def update_from_stock_data(new)
    temp_prev_52_high = week_52_high # save the prev before overwriting it
    temp_prev_52_low = week_52_low
    partial_update = self.class.map_stock_data(new)
    # To handle a migration issue where the prev high/low were causing saving errors. This is a good precaution to have moving forward.
    to_merge = if temp_prev_52_high.nil? || temp_prev_52_low.nil?
                 {}
               else
                 {prev_week_52_high: temp_prev_52_high,
                  prev_week_52_low: temp_prev_52_low}
               end
    full_update = partial_update.merge(to_merge)
    self.update(full_update)
  end

  # TODO test
  # Init a new watched stock from stock data interface
  # @param [StockDataInterface] new the data to populate a watched stock with
  # @return [WatchedStock] a new watched stock (unsaved)
  def self.new_from_stock_data(new)
    self.new(map_stock_data(new))
  end

  private

  # Helper to map StockDataInterface data to WatchedStock fields
  # @param [StockDataInterface] data
  # @return [Hash<String, String>] Mapped data
  def self.map_stock_data(data)
    {name: data.name,
     ticker: data.ticker,
     current_price: data.bid,
     last_price: data.last_price,
     change: data.change,
     change_percentage: data.change_percentage,
     volume: data.volume,
     average_volume: data.average_volume,
     open: data.open,
     high: data.high,
     low: data.low,
     close: data.close,
     prev_close: data.prev_close,
     week_52_high: data.week_52_high,
     week_52_low: data.week_52_low,
     prev_week_52_high: data.week_52_high,
     prev_week_52_low: data.week_52_low,
     asset_type: data.asset_type} unless data.nil?
  end
end
