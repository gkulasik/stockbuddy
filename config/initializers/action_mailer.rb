# To log exceptions rather than silently fail
ActionMailer::DeliveryJob.rescue_from(SparkPostRails::DeliveryException) do |exception|
  logger.info(exception)
end