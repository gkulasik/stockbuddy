require 'development_email_interceptor'
require 'staging_email_interceptor'

# Start up message for the email interceptors
def message(interceptor)
  "#{interceptor} is active - emails will be sent to: #{ENV['REDIRECT_EMAIL']} or whitelist"
end

## Depending on the environment/ENV var different interceptor will activate
if Rails.env.development?
  ActionMailer::Base.register_interceptor(DevelopmentEmailInterceptor)
  puts message('DevelopmentEmailInterceptor')
end
if BOOL_ENV.fetch('IS_STAGING')
  ActionMailer::Base.register_interceptor(StagingEmailInterceptor)
  puts message('StagingEmailInterceptor')
end
