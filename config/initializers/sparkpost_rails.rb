# SparkPost config
puts 'SparkPost configured...'
SparkPostRails.configure do |c|
  c.transactional = true
  c.html_content_only = true
end