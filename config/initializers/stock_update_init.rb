# Don't insert a job on start up if the table the Job works on does not exist
if ActiveRecord::Base.connection.table_exists? 'watched_stocks'
  puts 'StockUpdateJob marked to perform later...'
  StockUpdateJob.perform_later
end