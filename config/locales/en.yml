# Files in the config/locales directory are used for internationalization
# and are automatically loaded by Rails. If you want to use locales other
# than English, add the necessary files in this directory.
#
# To use the locales, use `I18n.t`:
#
#     I18n.t 'hello'
#
# In views, this is aliased to just `t`:
#
#     <%= t('hello') %>
#
# To use a different locale, set it with `I18n.locale`:
#
#     I18n.locale = :es
#
# This would use the information in config/locales/es.yml.
#
# To learn more, please read the Rails Internationalization guide
# available at http://guides.rubyonrails.org/i18n.html.

en:
  activerecord:
    attributes:
      stock_alert:
        type: "Alert type"
        stock_list_id: "Stock"
    models:
      phone_info: "Phone information"
  attributes:
    phone_number: "Phone number"
    carrier: "Carrier"
  models:
    stock_alerts:
      name:
        single: "Stock alert"
      subtypes:
        above_average_volume:
          select_text: "today's trade volume exceeds the average volume by..."
          view_text: "'s trade volume is %{formatter} above the average volume"
        new52_week_high:
          select_text: "a new 52 week high is reached"
          view_text: " reaches a new 52 week high"
        new52_week_low:
          select_text: "a new 52 week low is reached"
          view_text: " reaches a new 52 week low"
        price_decrease_amount:
          select_text: "the price decreases by X amount..."
          view_text: "'s price decreases by %{formatter}"
        price_decrease_percent:
          select_text: "the price decreased by X percent..."
          view_text: "'s price decreases by %{formatter}"
        price_greater_than:
          select_text: "the price is greater than..."
          view_text: "'s price is greater than %{formatter}"
        price_increase_amount:
          select_text: "the price increases by X amount..."
          view_text: "'s price increases by %{formatter}"
        price_increase_percent:
          select_text: "the price increases by X percent..."
          view_text: "'s price increases by %{formatter}"
        price_less_than:
          select_text: "the price is less than..."
          view_text: "'s price is less than %{formatter}"
        price_range:
          select_text: "the price is between..."
          view_text: "'s price is between %{min} and %{max}"
          validations:
            all_values_present:
              error: "Both a min and max must be provided. They must be positive."
            decimal_values:
              error: "Values must be decimals or integers"
            values_positive:
              error: "Both values must be positive."
            min_to_max_order:
              error: "Range must be from min to max. Order is incorrect."
      validations:
        positive_value: "must be a positive number"
    phone_infos:
      validations:
        format: 'must be 10 digits exactly and not start with a 0 or 1'
        sms_code_invalid: "SMS code does not match records. If this error continues please resend the code."
    user_settings:
      name:
        single: "Settings"
      validations:
        one_primary: "Must have only one primary phone number. If you are trying to disable SMS alerts use the alert setting below."
        has_phone: "If you want alerts via SMS please provide phone information. A primary phone is required."
    watched_stocks:
      name:
        single: "Watched stock"
    stock_lists:
      validations:
        no_duplicates: "This stock is already being watched by this user."
  controller:
    stock_alerts:
      flash:
        create:
          success: "Stock alert saved!"
        update:
          success: "Stock alert updated!"
        destroy:
          success: "Stock alert deleted."
      actions:
        reset_trigger:
          success: "Stock alert reset!"
          error: "Invalid reset token: Please verify you are using the latest stock alert notification or login to reset the trigger manually."
    stock_lists:
      flash:
        destroy:
          success: "Stock deleted from watch list!"
          error: "Stock not deleted from watch list. Something went wrong."
    stock_search:
      actions:
        add_watched_stock:
          success: "Added stock to watch list!"
          error:
            general: "Could not add stock to watch list."
            missing_data: "Could not add stock to watch list. Stock is missing required data."
    user_settings:
      flash:
        update:
          success: "Settings saved!"
          error: "Could not save settings."
      action:
        resend_sms_verification_code:
          success: "SMS code resent!"
          already_verified: "This phone has already been verified."
          error: "Uh oh! Looks like we ran into a problem. Please try again. If this continues please delete the phone entry and resubmit for verification."
    watched_stocks:
      flash:
        destroy:
          success: "Watched stock was successfully destroyed."
  mailers:
    sms_verify_mailer:
      verify:
        message: "Your StockBuddy sms verification code is: %{code}"
        subject: "Confirm SMS"
    stock_alert_mailer:
      notify_price_email:
        intro:
          no_alert_name: "Your alert for %{ticker}"
          has_alert_name: "%{alert_name} alert"
        subject:
          has_alert_name: "%{alert_name} alert triggered!"
          above_average_volume: "%{ticker} trade volume is above average!"
          new_52_week_high: "%{ticker} hit a new 52 week high!"
          new_52_week_low: "%{ticker} hit a new 52 week low!"
          price_decrease_amount: "%{ticker} price has decreased!"
          price_decrease_percent: "%{ticker} price has decreased!"
          price_greater_than: "%{ticker} price is greater than trigger!"
          price_increase_amount: "%{ticker} price has increased!"
          price_increase_percent: "%{ticker} price has increased!"
          price_less_than: "%{ticker} price is less than trigger!"
          price_range: "%{ticker} price is within trigger range!"
      notify_price_sms:
        subject: "%{ticker} alert"
        message:
          reset_link: "Reset alert: %{reset_link}"
          above_average_volume: "%{ticker} trading volume is {%} above average (trigger: {%})."
          new_52_week_high: "%{ticker} hit a new 52 week high of {$}."
          new_52_week_low: "%{ticker} hit a new 52 week low of {$}."
          price_decrease_amount: "%{ticker} price has decreased {$} (trigger: {$})."
          price_decrease_percent: "%{ticker} price has decreased {%} (trigger: {%})."
          price_greater_than: "%{ticker} price is greater than {$} (actual: {$})."
          price_increase_amount: "%{ticker} price has increased {$} (trigger: {$})."
          price_increase_percent: "%{ticker} price has increased {%} (trigger: {%})."
          price_less_than: "%{ticker} price is less than {$} (actual: {$})."
          price_range: "%{ticker} price is between {$}-{$} (actual: {$})."
  general:
    na: "N/A"
    greetings:
      welcome: "Welcome %{reference}!"
      hello: "Hello %{reference}!"
    fields:
      labels:
        first_name: "First name"
        last_name: "Last name"
        name: "Name"
        email: "Email"
        password: "Password"
        password_confirmation: "Password confirmation"
        current_password: "Current password"
        remember_me: "Remember me"
        message: "Message"
    actions:
      update:
        button: "Update"
      back: "Back"
      reset: "Reset"
      delete: "Delete"
      edit: "Edit"
      send: "Send"
    confirm:
      are_you_sure: "Are you sure?"
  views:
    devise:
      passwords:
        edit:
          fields:
            new_password: "New password"
            confirm_new_password: "Confirm new password"
            tip:
              minimum_length: "%{count} characters minimum"
          button:
            change_password: "Change my password"
          title: "Change your password"
        new:
          title: "Forgot your password?"
          buttons:
            send_reset_instructions: "Reset my password"
      confirmations:
        new:
          title: "Resend email confirmation"
          buttons:
            send_confirmation_instructions: "Resend confirmation"
      registrations:
        edit:
          title: "Your Profile"
          waiting_for_confirm: "Currently waiting confirmation for: %{unconfirmed_email}"
          fields:
            password:
              tip: "Leave blank if you don't want to change it"
            current_password:
              tip: "We need your current password to confirm your changes"
          cancel_my_account:
            tip: "Are you sure? This cannot be undone."
            text: "Cancel my account"
        new:
          agree_to: "By signing up, you agree to our"
          terms_of_service: "Terms of Service"
          title: "Sign up"
          captcha_failure: "reCAPTCHA verification failed, please try again."
      sessions:
        new:
          title: "Log in"
      shared:
        log_in: "Log in"
        sign_up: "Sign up"
        forgot_password: "Forgot your password?"
        didnt_receive_confirm_instructions: "Didn't receive confirmation instructions?"
        didnt_receive_unlock_instructions: "Didn't receive unlock instructions?"
        sign_in_with: "Sign in with %{provider}"
    layouts:
      menu:
        title: "Menu"
        options:
          home:
            label: "Home"
          stocks:
            label: "Stocks"
          watched_stocks:
            label: "Watched Stocks"
          stock_alerts:
            label: "Stock Alerts"
          my_account:
            label: "My Account"
          profile:
            label: "Profile"
          settings:
            label: "Settings"
          log_out:
            label: "Log out"
          tour:
            label: "Tour"
          contact:
            label: "Contact"
    shared:
      search:
        placeholder: "Lookup a Stock"
        button: "Search"
      more_asset_info:
        fields:
          current_price: "Current price"
          open: "Open"
          volume: "Volume"
          high: "High"
          change: "Change $"
          low: "Low"
          change_percentage: "Change %"
          close: "Close"
          previous_close: "Previous Close"
          52_week_high: "52 Week High"
          52_week_low: "52 Week Low"
        missing_data_warning: "Note: Certain assets may not have complete information."
    stock_alerts:
      form:
        title: "Alert me when..."
        optional_field: "Optional"
      edit:
        title: "Editing Stock Alert for %{name}"
      index:
        options:
          label: "Options"
        table:
          headers:
            column1: "Alert name"
            column2: "Trigger Condition"
            column3: "Triggered?"
        new:
          text: "New"
        filter:
          title: "Filter"
          filter1: "All"
          filter2: "Triggered"
          filter3: "Not triggered"
          filter4: "Clear Filters"
        title: "Your Stock Alerts"
      new:
        title: "New Stock Alert for %{name}"
    watched_stocks:
      watched_stock_display:
        watching_since: "Watching since: %{date_time}"
        alert: "alert"
        menu:
          actions: "Actions"
          add_alert: "Add Alert"
          view_alert: "View Alerts"
          remove: "Stop Watching"
          quick_view: "Quick view"
      index:
        title: "Your Watched Stocks"
        no_watched_stocks: "Uh oh! You don't have any watched stocks! Try adding some stocks using the search bar above."
        current_price:
          none: "N/A"
    stock_search:
      search:
        no_results: "No results for that search. Please try different search terms."
        watch_stock: "Watch"
        title: "Results"
    user_settings:
      edit:
        title: "Your Settings"
        alert_via_email:
          message: "Alert via Email"
          tooltip: "Send to account email address"
        alert_via_sms:
          message: "Alert via SMS (Primary phone)"
          tooltip: "Sends to primary phone"
        save: "Save"
      phone_infos:
        title: "Phone Settings for SMS"
        form:
          phone_number:
            placeholder: "Ex. 8471234567"
          code_to_verify:
            placeholder: "Get this via SMS"
            label: "Verify code"
          verify_phone: "Verify"
          resend_code: "Resend code"
          verified:
            text: "This phone has been confirmed for SMS"
          remove_phone: "Delete"
    contact_requests:
      title: "Contact Us"
      success: "Thank you for your message! We will contact you soon!"
      error: "Message not sent. Please verify all required information is completed."
  helpers:
    label:
      user_setting:
        phone_infos: "Phones"
      user:
        user_setting: "Settings"
        stock_lists: "Watching"
