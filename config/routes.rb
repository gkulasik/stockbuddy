Rails.application.routes.draw do

  # Nice looking URLs for pagination
  # Add concerns: :paginatable to a resource (with index action) and this will switch the routing for pages.
  concern :paginatable do
    get '(page/:page)', action: :index, on: :collection, as: ''
  end

  # Admin area and name spaces
  namespace :admin do
    resources :users
    resources :stock_alerts
    resources :watched_stocks
    resources :phone_infos
    resources :stock_lists
    resources :user_settings
    resources :above_average_volumes, controller: 'stock_alerts'
    resources :new52_week_highs, controller: 'stock_alerts'
    resources :new52_week_lows, controller: 'stock_alerts'
    resources :price_decrease_amounts, controller: 'stock_alerts'
    resources :price_decrease_percents, controller: 'stock_alerts'
    resources :price_greater_thans, controller: 'stock_alerts', type: 'PriceGreaterThan'
    resources :price_increase_amounts, controller: 'stock_alerts'
    resources :price_increase_percents, controller: 'stock_alerts'
    resources :price_less_thans, controller: 'stock_alerts'
    resources :price_ranges, controller: 'stock_alerts'

    root to: 'users#index'
  end

  ### PUBLIC PAGES
  get 'public/home'

  get 'public/terms_of_service'

  get 'public/tour'

  resources 'contacts', only: [:new, :create]
   match 'public/contact', to: 'contacts#new', via: 'get'
   match '/contacts', to: 'contacts#new', via: 'get'

  ### SEARCHING
  get 'search', to: 'stock_search#search', as: 'search'
  get 'query', to: 'stock_search#info_query', as: 'query'
  get 'add_watched_stock', to: 'stock_search#add_watched_stock', as: 'add_watched_stock'


  ### RESOURCE ROUTES
  resources :user_settings, only: [:edit, :update] do
    get 'resend_sms_verification_code'
  end

  resources :stock_list, only: [:destroy]

  devise_for :users, controllers: {registrations: 'devise_override/registrations'}

  resources :stock_alerts, except: [:show], concerns: :paginatable do
    # This route should allow for a more regular use of the param :id and makes the :reset_token part of the url and NOT a url param.
    match 'reset_trigger/(:reset_token)' => 'stock_alerts#reset_trigger', :as => :reset_trigger, via: :get, on: :member
  end

  resources :watched_stocks, only: [:index, :show, :destroy], concerns: :paginatable

  # STI StockAlert subclasses - they all route to StockAlertsController

  resources :above_average_volume, controller: 'stock_alerts'
  resources :new52_week_high, controller: 'stock_alerts'
  resources :new52_week_low, controller: 'stock_alerts'
  resources :price_less_than, controller: 'stock_alerts'
  resources :price_greater_than, controller: 'stock_alerts'
  resources :price_decrease_amount, controller: 'stock_alerts'
  resources :price_decrease_percent, controller: 'stock_alerts'
  resources :price_increase_amount, controller: 'stock_alerts'
  resources :price_increase_percent, controller: 'stock_alerts'
  resources :price_range, controller: 'stock_alerts'

  ### ROOT
  root 'public#home'

end
