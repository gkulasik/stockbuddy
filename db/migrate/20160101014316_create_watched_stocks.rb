class CreateWatchedStocks < ActiveRecord::Migration
  def change
    create_table :watched_stocks do |t|
      t.string :name
      t.string :ticker
      t.decimal :current_price

      t.timestamps null: false
    end
  end
end
