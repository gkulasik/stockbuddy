class CreateStockAlerts < ActiveRecord::Migration
  def change
    create_table :stock_alerts do |t|
      t.decimal :price_below
      t.decimal :price_above
      t.references :watched_stock, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
