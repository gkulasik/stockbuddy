class AddTriggeredToStockAlerts < ActiveRecord::Migration
  def change
    add_column :stock_alerts, :triggered, :boolean, default: false
  end
end
