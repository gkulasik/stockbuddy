class CreatePhoneInfos < ActiveRecord::Migration
  def change
    create_table :phone_infos do |t|
      t.string :phone_number
      t.string :carrier
      t.boolean :primary
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
