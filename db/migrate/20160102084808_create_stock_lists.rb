class CreateStockLists < ActiveRecord::Migration
  def change
    create_table :stock_lists do |t|
      t.references :user, index: true, foreign_key: true
      t.references :watched_stock, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
