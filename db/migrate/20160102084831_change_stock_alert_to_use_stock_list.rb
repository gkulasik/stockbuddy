class ChangeStockAlertToUseStockList < ActiveRecord::Migration
  def change
    change_table :stock_alerts do |t|
      t.remove :watched_stock_id
      t.references :stock_list
    end
  end
end
