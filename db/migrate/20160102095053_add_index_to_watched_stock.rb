class AddIndexToWatchedStock < ActiveRecord::Migration
  def change
    add_index :watched_stocks, :ticker, unique: true
    add_index :stock_lists, [:user_id, :watched_stock_id], unique: true
    add_index :phone_infos, :phone_number, unique: true
    remove_column :phone_infos, :primary
    add_column :phone_infos, :primary, :boolean, default: false
    
  end
end
