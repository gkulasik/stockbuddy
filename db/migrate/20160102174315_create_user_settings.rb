class CreateUserSettings < ActiveRecord::Migration
  def change
    create_table :user_settings do |t|
      t.boolean :alert_sms
      t.boolean :alert_email
      t.references :user, index: true, foreign_key: true, unique: true

      t.timestamps null: false
    end
    # move phone info to user settings
    remove_column :phone_infos, :user_id
    add_column :phone_infos, :user_setting_id, :integer, index: true, foreign_key: true
  end
end
