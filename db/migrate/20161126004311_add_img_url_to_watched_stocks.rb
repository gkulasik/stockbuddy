class AddImgUrlToWatchedStocks < ActiveRecord::Migration
  def change
    add_column :watched_stocks, :image_url, :string
  end
end
