class AddResetTokenToStockAlert < ActiveRecord::Migration
  def change
    add_column :stock_alerts, :reset_token, :string
  end
end
