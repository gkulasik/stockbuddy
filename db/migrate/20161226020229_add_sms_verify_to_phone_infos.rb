class AddSmsVerifyToPhoneInfos < ActiveRecord::Migration
  def change
    add_column :phone_infos, :verify_code, :string
    add_column :phone_infos, :confirmed, :boolean, default: false
  end
end
