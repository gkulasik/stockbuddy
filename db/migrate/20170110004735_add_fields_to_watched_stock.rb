class AddFieldsToWatchedStock < ActiveRecord::Migration
  def change
    add_column :watched_stocks, :last_price, :decimal
    add_column :watched_stocks, :change, :decimal
    add_column :watched_stocks, :change_percentage, :decimal
    add_column :watched_stocks, :volume, :integer
    add_column :watched_stocks, :average_volume, :integer
    add_column :watched_stocks, :open, :decimal
    add_column :watched_stocks, :high, :decimal
    add_column :watched_stocks, :low, :decimal
    add_column :watched_stocks, :close, :decimal
    add_column :watched_stocks, :prev_close, :decimal
    add_column :watched_stocks, :week_52_high, :decimal
    add_column :watched_stocks, :week_52_low, :decimal
    add_column :watched_stocks, :prev_week_52_high, :decimal
    add_column :watched_stocks, :prev_week_52_low, :decimal
    add_column :watched_stocks, :asset_type, :string
  end
end
