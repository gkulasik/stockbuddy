class AddFieldsToStockAlert < ActiveRecord::Migration
  def change
    add_column :stock_alerts, :value, :string
    add_column :stock_alerts, :alert_name, :string
    add_column :stock_alerts, :type, :string
  end
end
