class RemovePriceAboveAndPriceBelowFromStockAlerts < ActiveRecord::Migration
  def change
    remove_column :stock_alerts, :price_above
    remove_column :stock_alerts, :price_below
  end
end
