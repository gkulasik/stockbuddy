class AddUserIdUniqueIndexToUserSettings < ActiveRecord::Migration
  def change
    # Index on user_id was not unique for some reason - now it is.
    remove_index :user_settings, :user_id
    add_index :user_settings, :user_id, unique: true
  end
end
