# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170520144030) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "phone_infos", force: :cascade do |t|
    t.string   "phone_number"
    t.string   "carrier"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.boolean  "primary",         default: false
    t.integer  "user_setting_id"
    t.string   "verify_code"
    t.boolean  "confirmed",       default: false
  end

  add_index "phone_infos", ["phone_number"], name: "index_phone_infos_on_phone_number", unique: true, using: :btree

  create_table "stock_alerts", force: :cascade do |t|
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.boolean  "triggered",     default: false
    t.integer  "stock_list_id"
    t.string   "reset_token"
    t.string   "value"
    t.string   "alert_name"
    t.string   "type"
  end

  create_table "stock_lists", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "watched_stock_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "stock_lists", ["user_id", "watched_stock_id"], name: "index_stock_lists_on_user_id_and_watched_stock_id", unique: true, using: :btree
  add_index "stock_lists", ["user_id"], name: "index_stock_lists_on_user_id", using: :btree
  add_index "stock_lists", ["watched_stock_id"], name: "index_stock_lists_on_watched_stock_id", using: :btree

  create_table "user_settings", force: :cascade do |t|
    t.boolean  "alert_sms"
    t.boolean  "alert_email"
    t.integer  "user_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "user_settings", ["user_id"], name: "index_user_settings_on_user_id", unique: true, using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.string   "first_name"
    t.string   "last_name"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.boolean  "admin",                  default: false
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "watched_stocks", force: :cascade do |t|
    t.string   "name"
    t.string   "ticker"
    t.decimal  "current_price"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "image_url"
    t.decimal  "last_price"
    t.decimal  "change"
    t.decimal  "change_percentage"
    t.integer  "volume"
    t.integer  "average_volume"
    t.decimal  "open"
    t.decimal  "high"
    t.decimal  "low"
    t.decimal  "close"
    t.decimal  "prev_close"
    t.decimal  "week_52_high"
    t.decimal  "week_52_low"
    t.decimal  "prev_week_52_high"
    t.decimal  "prev_week_52_low"
    t.string   "asset_type"
  end

  add_index "watched_stocks", ["ticker"], name: "index_watched_stocks_on_ticker", unique: true, using: :btree

  add_foreign_key "stock_lists", "users"
  add_foreign_key "stock_lists", "watched_stocks"
  add_foreign_key "user_settings", "users"
end
