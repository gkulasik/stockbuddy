# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Helper to variably do a look up and create if the result is nil.
# Throws exception on failure to save
# @param [Object] object to create if not exists
# @param [Hash] search to execute to determine if the object exists already. Ex. {email: "test@example.com"}
def create_if_not_exists(object, search = {})
  found = object.class.name.constantize.find_by(search)
  if found.nil?
    object.save!
    puts object.errors.full_messages unless object.errors.full_messages.empty?
  end
end

#####
##### NOTES
#####
# - seeds.rb should be idempotent - this should check that the data it is seeding is present or not and if not then only populate the DB. This will be simpler and avoid many issues with deployments.

##
### ADMIN SETUP
##
admin_password = Security.generate_random_token(5) # generate 2 tokens to ensure security of admin account
admin_tokens = Security.generate_random_token(5)
admin_email = 'rampitupdevelopment@gmail.com'
admin = User.new(first_name: 'Admin', last_name: 'User', password: admin_password, password_confirmation: admin_password, email: admin_email, reset_password_token: admin_tokens, confirmation_token: admin_tokens, admin: true)
create_if_not_exists(admin, {email: admin.email})
puts 'Admin setup or already exists...'


