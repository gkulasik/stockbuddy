require 'optparse'
require 'ostruct'
require 'byebug' # for debugging only


MASTER_BRANCH = 'master'

# CONFIG Environment setup - match your remote name (GIT) to the heroku app name.
ENVIRONMENTS = {
    production: 'stockbuddyapp',
    staging: 'stockbuddyapp-test'
}
# CONFIG Set up structure to hold all options
# Change defaults if you want here
OPTIONS = {
    environment: nil, # Remote/environment
    run_migration: true, # Should run migration?
    run_backup: false, # Should run backup?
    branch: MASTER_BRANCH, # Which branch to use?
    leave_maintenance_on: false, # Should leave maintenance mode on?
    run_seed: true # Should seed the db?
}

# Arguments for the deploy script
class DeployScriptArgs

  def self.parse(args)
    options = OPTIONS
    # Define parameters and usage
    option_parser = OptionParser.new do |opts|
      # Access options/help with --help
      opts.banner = 'Usage: ruby deployroku.rb [options]'

      # NOTE:
      # String opt is autocompleted. You can just put the first letter of the environment (as long as they are unique first letters. ex. staging, qa, production)
      opts.on('-e [ENV]', '--env [ENV]', String, ENVIRONMENTS.map { |key, value| key.to_sym },
              "Set the remote to push to. e.g. '-e staging' Default: #{options[:environment]}. REQUIRED if no default.") do |env|
        options[:environment] = env
      end

      opts.on('-b [BRANCH]', '--branch [BRANCH]', String,
              "Select a branch to push. e.g. '-b test-feature' Default: #{MASTER_BRANCH}") do |branch|
        options[:branch] = branch.downcase
      end

      opts.on('-m', '--run_migration',
              "Run `heroku run rake db:migrate`. #{runs_by_default?(options[:run_migration])}") do |mig|
        options[:run_migration] = evaluate_bool_param_value(options[:run_migration], mig)
      end

      opts.on('-s', '--run_seed_db',
              "Run `heroku run rake db:seed`. #{runs_by_default?(options[:run_seed])}") do |seed|
        options[:run_seed] = evaluate_bool_param_value(options[:run_seed], seed)
      end

      opts.on('-u', '--run_backup',
              "Run `heroku pgbackups:capture`. #{runs_by_default?(options[:run_backup])}") do |bu|
        options[:run_backup] = evaluate_bool_param_value(options[:run_backup], bu)
      end

      opts.on('-l', '--leave_maintenance_on', "Do not turn off maintenance mode at the end of the deploy. Default: #{options[:leave_maintenance_on]}") do |maint|
        options[:leave_maintenance_on] = evaluate_bool_param_value(options[:leave_maintenance_on], maint)
      end
    end

    # Parse the args
    option_parser.parse!(args)
    # Validate the args
    validate_options(options)
    options
  end

  # @param [Hash] options Options to validate
  # @return [Nil] Throws exception if there is a validation error
  def self.validate_options(options)
    if options[:environment] == nil || !ENVIRONMENTS.key?(options[:environment])
      raise 'Missing environment.'
    end
  end

  # @param [Boolean] runs_by_default
  # @return [String] Test to say whether X runs by default or not
  def self.runs_by_default?(runs_by_default)
    if runs_by_default
      'Runs by default. Set flag to disable.'
    else
      'Does not run by default. Set flag to enable.'
    end
  end


  # Evaluate params against default values
  # Needs: Allow input params to specify all aspects - but allow defaults to save typing time. Manually input params should always override defaults somehow.
  # Evaluation:
  # Default = Default value set in OPTIONS
  # Input = Parameter passed to script (T = flag passed, F = flag not passed)
  # Default | Input | Result
  #   T     |   T   |   F
  #   F     |   F   |   F
  #   T     |   F   |   T
  #   F     |   T   |   T
  # @param [Boolean] default
  # @param [Boolean] input
  # @return [Boolean] the final param value
  def self.evaluate_bool_param_value(default, input)
    if default == input
      false
    else
      true
    end
  end
end


# Actual script doing work starting here VVV
options = DeployScriptArgs.parse(ARGV)
puts 'Everything committed and ready to deploy for branch: ' + options[:branch].to_s + '? (y/n)'
option = STDIN.gets.chomp
if option.downcase.strip != 'y'
  puts 'Please commit first than rerun the script. Aborting...'
  exit(0)
end

# Confirm deployment
puts '----------------------------------------------------'
puts 'Deployment Confirmation'
puts '----------------------------------------------------'
puts "Branch: #{options[:branch].upcase}"
puts "Environment/Remote: #{options[:environment].upcase}"
puts "Run Migrations? #{(options[:run_migration]).to_s.upcase}"
puts "Run Backup? #{(options[:run_backup]).to_s.upcase}"
puts "Run rake db:seed? #{(options[:run_seed]).to_s.upcase}"
puts "Leave maintenance on? #{(options[:leave_maintenance_on]).to_s.upcase}"
puts '----------------------------------------------------'
puts 'OK to proceed? (y/n)'
option = STDIN.gets.chomp
if option.downcase.strip != 'y'
  puts 'Aborting...'
  exit(0)
end

# Format the selected environment/app for Figaro and Heroku commands
selected_app = ' -a ' + ENVIRONMENTS[options[:environment]]

# Begin deployment
begin
  # Always deploy in maintenance mode.
  puts '----------------------------------------------------'
  puts 'Enabling Maintenance Mode'
  puts '----------------------------------------------------'
  system 'heroku maintenance:on' + selected_app

  # No option to disable - if you change a production value it's expected you want to change it.
  # Else Figaro is idempotent - no changes unless user makes changes.
  puts '----------------------------------------------------'
  puts 'Setting ENV Variables'
  puts '----------------------------------------------------'
  system 'figaro heroku:set -e ' + options[:environment].to_s + selected_app

  # Deploy via GIT
  # Selects branch to push based on input.
  puts '----------------------------------------------------'
  puts 'Deploying to Heroku (Pushing)'
  puts '----------------------------------------------------'
  push_var = options[:branch].to_s != MASTER_BRANCH ? ':' + MASTER_BRANCH : ''
  system 'git push ' + options[:environment].to_s + ' ' + options[:branch].to_s + push_var

  # Backup DB before migrations/seeds (back it up exactly like it is now)
  if options[:run_backup]
    puts '----------------------------------------------------'
    puts 'Running Backup'
    puts '----------------------------------------------------'
    system 'heroku pgbackups:capture --expire ' + selected_app
  end

  # Run migrations on the DB
  if options[:run_migration]
    puts '----------------------------------------------------'
    puts 'Running Migrations'
    puts '----------------------------------------------------'
    system 'heroku run rake db:migrate' + selected_app
  end

  # Seed the DB
  if options[:run_seed]
    puts '----------------------------------------------------'
    puts 'Running db:seed'
    puts '----------------------------------------------------'
    system 'heroku run rake db:seed' + selected_app
  end

  # Turn off maintenance mode
  # If option is set to true then maintenance mode is left on. This allows dev to run other migration steps outside of this script if needed.
  # Dev must disable maintenance mode themselves with 'heroku maintenance:off -a [your app name]'
  unless options[:leave_maintenance_on]
    puts '----------------------------------------------------'
    puts 'Disabling Maintenance Mode'
    puts '----------------------------------------------------'
    system 'heroku maintenance:off' + selected_app
  end

  puts '----------------------------------------------------'
  puts 'Deploy Completed!'
  puts '----------------------------------------------------'

  # Friendly reminder that maintenance mode is still on for the dev.
  if options[:leave_maintenance_on]
    puts 'NOTE: Maintenance mode is still on!'
  end
rescue
  puts 'Issue during deploy. Site may not be updated. Please resolve issues and try again.'
end 


