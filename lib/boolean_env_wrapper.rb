class BooleanEnvWrapper < SimpleDelegator
  # @param [String] key the ENV key to retrieve
    # @return [Boolean] t/f based on the string it is interpreting
    def fetch(key)
    begin
      val = super
    rescue Exception => e
      puts e.message + e.backtrace.inspect
      return false
    end
    return false if val == nil
    return true if val.to_s.downcase == 'true'
    return false if val.to_s.downcase == 'false'
    return val
  end
end