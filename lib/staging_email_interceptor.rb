# Interceptor for Staging email
class StagingEmailInterceptor
  # @param [Message] message the email message
  # @return [Message] The modified message
  def self.delivering_email(message)
    # Allows emails to the whitelist to pass (does an intersect)
    if (STAGING_EMAIL_WHITELIST & message.to).empty?
      # Adjusts subject to be the recipient and subject concatenated
      # Changes the recipient to the environment declared redirect email
      message.subject = "#{message.to.first}: #{message.subject}"
      message.to = [ENV['REDIRECT_EMAIL']]
    end
  end
end
