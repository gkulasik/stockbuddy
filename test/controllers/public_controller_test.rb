require 'test_helper'

class PublicControllerTest < ActionController::TestCase

  ## Home tests
  test 'should get home' do
    get :home
    assert_not_nil assigns(:image_options)
    assert_response :success
  end

  ## Terms of Service tests
  test 'should get terms_of_service' do
    get :terms_of_service
    assert_response :success
  end

  ## Tour tests
  test 'should get tour' do
    get :tour
    assert_response :success
  end

end
