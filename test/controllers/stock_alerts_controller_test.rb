require 'test_helper'

class StockAlertsControllerTest < ActionController::TestCase

  ## TODO
  # Use new gems to make better tests (FactoryGirl, Shoulda, Capybara)
  setup do
    @stock_alert = stock_alerts(:one1)
    @controller_name = 'stock_alerts_controller'
    sign_in users(:correct)
  end

  ## Index tests
  test 'should get index for all a users stock alerts' do
    get :index
    assert_response :success
    assert_not_nil assigns(:stock_alerts)
    #assert assigns(:stock_alerts)
  end

  test 'should get index for one watched stocks stock alerts' do
    get :index, alert_for: stock_lists(:one_correct)
    assert_response :success
    assert_not_nil assigns(:stock_alerts)
    assert(assigns(:stock_alerts).include?(stock_alerts(:one1)))
    assert_equal(1, assigns(:stock_alerts).length)
  end

  test 'ensure index is locked' do
    sign_out users(:correct)
    not_authorized_helper('index', @controller_name, :index, {}, :get)
  end

  ## New tests
  test 'should get new' do
    get :new, stock_list_id: stock_lists(:one_correct).id
    assert_response :success
    assert_not_nil assigns(:stock_alert) # the started stock alert object
  end

  test 'ensure new is locked' do
    sign_out users(:correct)
    not_authorized_helper('new', @controller_name, :new, {}, :get)
  end

  ## Create tests
  test 'should create stock_alert' do
    get :new, stock_list_id: stock_lists(:one_correct).id
    assert_difference('StockAlert.count') do
      post :create, stock_alert: {type: 'PriceGreaterThan', value: '1000', stock_list_id: stock_lists(:one_correct).id}
    end
    assert_not_nil assigns(:stock_alert)
    assert_equal I18n.t('controller.stock_alerts.flash.create.success'), flash[:notice]
    assert_redirected_to stock_alerts_path
  end

  test 'should not create stock_alert' do
    get :new, stock_list_id: stock_lists(:one_correct).id
    assert_no_difference('StockAlert.count') do
      post :create, stock_alert: {type: 'PriceGreaterThan', value: '-100', stock_list_id: stock_lists(:one_correct).id} # value must be positive
      assert_not_nil(:stock)
      post :create, stock_alert: {value: '100', stock_list_id: stock_lists(:one_correct).id} # type must be present
      assert_not_nil(:stock)
      # post :create, stock_alert: {type: 'PriceGreaterThan', value: '100', stock_list_id: ''}
      # TODO should we nicely handle a case where the stock_list_id is missing in this case?
    end
    assert_response :success # go back to new on error - no change in count ^
  end

  test 'ensure create is locked' do
    sign_out users(:correct)
    not_authorized_helper('create', @controller_name, :create, {value: '10', stock_list_id: stock_lists(:one_correct).id}, :post)
  end

  ## Edit tests
  test 'should get edit' do
    get :edit, id: @stock_alert
    assert_not_nil assigns(:stock)
    assert_not_nil assigns(:stock_alert)
    assert_response :success
  end

  test 'ensure edit is locked' do
    sign_out users(:correct)
    not_authorized_helper('edit', @controller_name, :edit, {id: @stock_alert.id}, :get)
  end

  ## Update tests
  test 'should update stock_alert' do
    patch :update, id: @stock_alert, stock_alert: {value: @stock_alert.value, stock_list_id: @stock_alert.stock_list.id}
    assert_not_nil assigns(:stock)
    assert_not_nil assigns(:stock_alert)
    assert_redirected_to stock_alerts_path
    assert_equal I18n.t('controller.stock_alerts.flash.update.success'), flash[:notice]
  end

  test 'should not update stock_alert with bad data' do
    # negative value
    patch :update, id: @stock_alert, stock_alert: {value: '-10.00', stock_list_id: @stock_alert.stock_list_id}
    assert_response 200 # go to :edit
    assert_nil flash[:notice]
    assert_not_empty assigns(:stock_alert).errors.messages
    # missing stock_list_id
    patch :update, id: @stock_alert, stock_alert: {value: 100, stock_list_id: ''}
    assert_response 200 # go to :edits
    assert_nil flash[:notice]
    assert_not_empty assigns(:stock_alert).errors.messages
  end

  test 'ensure update is locked' do
    sign_out users(:correct)
    not_authorized_helper('update', @controller_name, :update, {id: @stock_alert, stock_alert: {value: 10.00, stock_list_id: 0}}, :patch)
  end

  ## Destroy tests
  test 'should destroy stock_alert' do
    assert_difference('StockAlert.count', -1) do
      delete :destroy, id: @stock_alert
    end
    assert_not_nil assigns(:stock)
    assert_not_nil assigns(:stock_alert)
    assert_redirected_to stock_alerts_path
    assert_equal I18n.t('controller.stock_alerts.flash.destroy.success'), flash[:notice]
  end

  test 'ensure delete is locked' do
    sign_out users(:correct)
    not_authorized_helper('delete', @controller_name, :destroy, {id: @stock_alert.id}, :delete)
  end


  ## Reset_trigger tests
  test 'reset trigger signed in' do
    triggered = stock_alerts(:triggered)
    # get stock_alert_reset_trigger_url(triggered, reset_token: triggered.reset_token)
    get :reset_trigger, id: triggered.id, reset_token: triggered.reset_token
    assert_redirected_to stock_alerts_path
    assert_equal I18n.t('controller.stock_alerts.actions.reset_trigger.success'), flash[:notice]
    assert_not StockAlert.find_by(id: triggered.id).triggered
  end

  test 'reset trigger not signed in' do
    sign_out users(:correct)
    triggered = stock_alerts(:triggered)
    # get stock_alert_reset_trigger_url(triggered, reset_token: triggered.reset_token)
    get :reset_trigger, id: triggered.id, reset_token: triggered.reset_token
    assert_redirected_to root_path
    assert_equal I18n.t('controller.stock_alerts.actions.reset_trigger.success'), flash[:notice]
    assert_not StockAlert.find_by(id: triggered.id).triggered
  end

  test 'reset trigger missing trigger/ incorrect and signed in' do
    triggered = stock_alerts(:triggered)
    # get stock_alert_reset_trigger_url(triggered, reset_token: triggered.reset_token)
    get :reset_trigger, id: triggered.id, reset_token: 'wrong_token'
    assert_redirected_to stock_alerts_path
    assert_equal I18n.t('controller.stock_alerts.actions.reset_trigger.error'), flash[:alert]
    assert StockAlert.find_by(id: triggered.id).triggered
  end

  test 'reset trigger missing trigger/ incorrect and not signed in' do
    sign_out users(:correct)
    triggered = stock_alerts(:triggered)
    # get stock_alert_reset_trigger_url(triggered, reset_token: triggered.reset_token)
    get :reset_trigger, id: triggered.id, reset_token: 'wrong_token'
    assert_redirected_to root_path
    assert_equal I18n.t('controller.stock_alerts.actions.reset_trigger.error'), flash[:alert]
    assert StockAlert.find_by(id: triggered.id).triggered
  end

end
