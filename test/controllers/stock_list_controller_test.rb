require 'test_helper'

class StockListControllerTest < ActionController::TestCase

  setup do
    @fresh_stock_list = StockList.create(user: users(:correct), watched_stock: watched_stocks(:unassigned))
    @controller_name = 'stock_list_controller'
  end

  ## Destroy tests
  test 'should destroy stock list entry' do
    sign_in @fresh_stock_list.user
    assert_difference('StockList.count', -1) do
      delete :destroy, id: @fresh_stock_list.id
    end
    assert_redirected_to root_path
    assert_not_nil assigns(:stock_list)
    assert_equal I18n.t('controller.stock_lists.flash.destroy.success'), flash[:success]
  end

  test 'ensure destroy is locked' do
    not_authorized_helper('destroy', @controller_name, :destroy, {id: @fresh_stock_list.id}, :delete)
  end

  test 'should not destroy - bad input' do
    sign_in @fresh_stock_list.user
    begin
      get :destroy, id: 'fake id'
      fail('Should have erred - ActiveRecord::RecordNotFound')
    rescue ActiveRecord::RecordNotFound
      # pass
    end
  end
end
