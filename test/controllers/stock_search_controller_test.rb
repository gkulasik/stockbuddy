require 'test_helper'

# TESTING NOTE: If any of these fail be sure it may not be due to lack of internet connectivity. Retry if not sure of the failure cause.

# START

class StockSearchControllerTest < ActionController::TestCase
  setup do
    @controller_name = 'stock_search_controller'
  end

  test 'should get search' do
    sign_in users(:correct)
    get :search
    assert_response :success
    assert_not_nil(assigns(:symbols))
    assert_equal 0, assigns(:symbols).length
  end

  test 'should get search query' do
    sign_in users(:correct)
    get :search, q: 'Microsoft'
    assert_response :success
    assert_not_nil(assigns(:symbols))
    assert_equal 1, assigns(:symbols).length
  end

  test 'ensure search is locked' do
    not_authorized_helper('search', @controller_name, :search, {})
    not_authorized_helper('search', @controller_name, :search, {q: 'TSLA'})
  end

  test 'add watched stock missing query' do
    sign_in users(:correct)
    get :add_watched_stock
    assert_response :success
    assert_equal I18n.t('controller.stock_search.actions.add_watched_stock.error.general'), flash[:alert]
  end

  test 'add watched stock success' do
    sign_in users(:correct)
    get :add_watched_stock, to_watch: 'TSLA'
    assert_response :success
    assert_equal 'Added stock to watch list!', flash[:success]
  end

  test 'add watched stock does not have unique index exception' do
    sign_in users(:correct)
    assert_difference('WatchedStock.count') do
      get :add_watched_stock, to_watch: 'MSFT'
    end
    assert_response :success
    assert_equal 'Added stock to watch list!', flash[:success]
    # attempting to add the stock again should not error out and should not create a new watched stock
    assert_no_difference('WatchedStock.count') do
      get :add_watched_stock, to_watch: 'MSFT'
    end
    assert_response :success
    assert_equal 'Added stock to watch list!', flash[:success]
  end

  test 'ensure add_watched_stock is locked' do
    not_authorized_helper('add_watched_stock', @controller_name, :add_watched_stock, {})
    not_authorized_helper('add_watched_stock', @controller_name, :add_watched_stock, {to_watch: 'TSLA'})
  end

end
