require 'test_helper'

class UserSettingsControllerTest < ActionController::TestCase

  setup do
    @user_setting = user_settings(:correct)
    @controller_name = 'user_settings_controller'
  end

  ### edit tests

  test 'should get edit for user settings' do
    sign_in users(:correct)
    get :edit, id: 'nil' # dont need id (get by signed in user) but need the value for happy routing
    assert_response :success
    assert_not_nil assigns(:settings)
    assert_not_nil assigns(:settings).phone_infos
  end

  test 'ensure edit is locked' do
    not_authorized_helper('edit', @controller_name, :edit, {id: 'nil'})
  end

  ### update tests

  test 'should update user settings' do
    sign_in users(:correct)
    patch :update, id: @user_setting, user_setting: {alert_sms: true, phone_infos: {}}
    assert_redirected_to edit_user_setting_path(@user_setting)
  end

  test 'should update user settings with phone info' do
    sign_in users(:correct)
    patch :update, id: @user_setting, user_setting: {alert_sms: "1", phone_infos_attributes: {"0" => {phone_number: '8473211234', carrier: 't-mobile', primary: "0", user_setting_id: @user_setting.id}}}
    assert_redirected_to edit_user_setting_path(@user_setting)
  end

  test 'ensure update is locked' do
    not_authorized_helper('update', @controller_name, :update, {id: 'nil', user_settings: {alert_sms: true}}, :patch)
  end

  ### resend_sms_verification_code tests

  test 'should resend the sms code' do
    sign_in users(:correct)
    phone = phone_infos(:correct_one)
    before_code = phone.verify_code
    get :resend_sms_verification_code, user_setting_id: @user_setting, phone: phone.id
    assert_redirected_to edit_user_setting_path(@user_setting)
    assert_equal 'SMS code resent!', flash[:success]
    assert_not_equal PhoneInfo.find_by(id: phone.id).verify_code, before_code, 'should have reset the code'
  end

  test 'should not resend the sms code due to already verified' do
    sign_in users(:correct)
    phone = phone_infos(:correct_one)
    phone.confirmed = true
    phone.save!
    before_code = phone.verify_code
    get :resend_sms_verification_code, user_setting_id: @user_setting, phone: phone.id
    assert_redirected_to edit_user_setting_path(@user_setting)
    assert_equal 'This phone has already been verified.', flash[:info]
    assert_equal PhoneInfo.find_by(id: phone.id).verify_code, before_code, 'should NOT have reset the code'
  end

  test 'should not resend the sms code and throw an error' do
    sign_in users(:correct)
    phone = phone_infos(:correct_one)
    before_code = phone.verify_code
    get :resend_sms_verification_code, user_setting_id: @user_setting, phone: ''
    assert_redirected_to edit_user_setting_path(@user_setting)
    assert_equal 'Uh oh! Looks like we ran into a problem. Please try again. If this continues please delete the phone entry and resubmit for verification.', flash[:alert]
    assert_equal PhoneInfo.find_by(id: phone.id).verify_code, before_code, 'should NOT have reset the code'
    get :resend_sms_verification_code, user_setting_id: @user_setting, phone: '1'
    assert_redirected_to edit_user_setting_path(@user_setting)
    assert_equal 'Uh oh! Looks like we ran into a problem. Please try again. If this continues please delete the phone entry and resubmit for verification.', flash[:alert]
    assert_equal PhoneInfo.find_by(id: phone.id).verify_code, before_code, 'should NOT have reset the code'
  end

  test 'ensure resend_sms_verification_code is locked' do
    phone = phone_infos(:correct_one)
    not_authorized_helper('resend_sms_verification_code', @controller_name, :resend_sms_verification_code, {user_setting_id: @user_setting.id, phone: phone.id})
  end

end
