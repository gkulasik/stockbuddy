require 'test_helper'

class WatchedStocksControllerTest < ActionController::TestCase

  setup do
    @watched_stock = watched_stocks(:one)
    @controller_name = 'watched_stock_controller'
    @watched_stock_no_stock_lists = watched_stocks(:no_lists)
  end

  test 'should get index' do
    sign_in users(:correct)
    get :index
    assert_response :success
    assert_not_nil assigns(:watched_stocks)
  end

  test 'ensure index is locked' do
    not_authorized_helper('index', @controller_name, :index, {}, :get)
  end

  test 'should not destroy watched_stock with stock_lists' do
    sign_in users(:correct)
    # Test DB protections
    begin
    assert_difference('WatchedStock.count', -1) do
      delete :destroy, id: @watched_stock
    end
      fail('Should have had foreign key exception (stock lists reference this watched_stock')
    rescue ActiveRecord::InvalidForeignKey
      # pass
    end
  end

  test 'should destroy watched_stock' do
    sign_in users(:correct)
    # test deletion in general
    assert_difference('WatchedStock.count', -1) do
      delete :destroy, id: @watched_stock_no_stock_lists
    end
    assert_redirected_to watched_stocks_url
    assert_equal 'Watched stock was successfully destroyed.', flash[:notice]
  end

  test 'ensure destroy is locked' do
    not_authorized_helper('destroy', @controller_name, :destroy, {id: @watched_stock_no_stock_lists}, :delete)
  end

end
