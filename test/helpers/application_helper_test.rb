require 'test_helper'
class ApplicationHelperTest < ActionView::TestCase

  test 'flash notice gives correct output' do
    # Add any new flash types to sym_and_expected
    sym_and_expected = {notice: 'alert-box success', success: 'alert-box success', error: 'alert-box alert', alert: 'alert-box alert'}
    sym_and_expected.each_pair do |key, value|
      assert_equal value, flash_class(key)
    end
  end

  test 'format created at' do
    test_date = DateTime.ordinal(2001,34) # 34th day of 2001, Feb 3rd, 2001
    test_text = 'February 3, 2001'
    formatted_now = test_date.strftime('%B %-d, %Y')
    obj = WatchedStock.new(created_at: test_date)

    assert_equal formatted_now, formatted_created_at(obj) # Test formula
    assert_equal test_text, formatted_created_at(obj) # test actual (by hand)
    assert_equal '', formatted_created_at(nil)
  end

  test 'formatted currency' do
    amount = 1
    formatted_amount = number_to_currency(amount)
    test_text = "$#{amount}.00"

    assert_equal formatted_amount, formatted_currency(amount)
    assert_equal test_text, formatted_currency(amount)
    assert_nil formatted_currency(nil)
  end
end