# Preview all emails at http://localhost:3000/rails/mailers/sms_verify_mailer
class SmsVerifyMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/sms_verify_mailer/verify
  def verify
    SmsVerifyMailer.verify
  end

end
