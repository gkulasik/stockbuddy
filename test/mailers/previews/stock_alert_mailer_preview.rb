# Preview all emails at http://localhost:3000/rails/mailers/stock_alert_mailer
class StockAlertMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/stock_alert_mailer/notify
  def notify
    StockAlertMailer.notify
  end

end
