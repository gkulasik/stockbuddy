require 'test_helper'

class SmsVerifyMailerTest < ActionMailer::TestCase
  test 'verify sms mailer' do
    email = '1234567890@test.com'
    code = '123'
    mail = SmsVerifyMailer.verify(email, code)
    assert_equal 'Confirm SMS', mail.subject
    assert_equal [email], mail.to
    assert_equal [ENV['MAILER_FROM']], mail.from
    assert_match 'Your StockBuddy sms verification code is: ' + code, mail.body.encoded
  end

end
