require 'test_helper'

class StockAlertMailerTest < ActionMailer::TestCase

  setup do
    # TODO clean up the tests/fixtures to provide better naming
    @stock = watched_stocks(:one)
    @alert = stock_alerts(:one1)
    # TODO figure out how to get the url method to work - for now this is sufficient
    @link = "http://test.host/stock_alerts/#{@alert.id}/reset_trigger" #received from: stock_alert_reset_trigger_url(alert, reset_token: alert.reset_token)
  end

  test 'sms stock alert mailer' do
    phone_sms_email = '1232343456@test.net'

    # Build test message
    link_message = I18n.t('mailers.stock_alert_mailer.notify_price_sms.message.reset_link', reset_link: @link)
    main_message = Formatter.format_text(t("mailers.stock_alert_mailer.notify_price_sms.message.#{@alert.class.partial_name}", ticker: @stock.ticker), @alert.short_view_params)
    message = main_message # + ' <br/> ' + link_message This removed due to the link being clicked by TMobile

    mail = StockAlertMailer.notify_price_sms(phone_sms_email, @stock, @alert)
    assert_equal I18n.t('mailers.stock_alert_mailer.notify_price_sms.subject', ticker: @stock.ticker), mail.subject
    assert_equal [phone_sms_email], mail.to
    assert_equal [ENV['MAILER_FROM']], mail.from
    assert_equal message, mail.body.encoded
  end

  test 'email stock alert mailer' do
    mail = StockAlertMailer.notify_price_email(users(:correct), @stock, @alert)
    assert_equal I18n.t('mailers.stock_alert_mailer.notify_price_email.subject.price_greater_than', ticker: @stock.ticker), mail.subject
    assert_equal [users(:correct).email], mail.to
    assert_equal [ENV['MAILER_FROM']], mail.from
    # TODO test content of the emails with fixtures
    # assert_equal read_fixture('email_stock_alert...').join, mail.body.encoded
  end

end
