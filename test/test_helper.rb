ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require 'capybara'
require 'capybara/dsl'

class ActiveSupport::TestCase
  # Add more helper methods to be used by all tests here...

  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  include FactoryGirl::Syntax::Methods

  # Test not authorized helper
  # Allows quick verification that users are not allow to access a certain action without
  # being signed in
  # Params: action_name ('edit'), controller (x_controller), action (:edit), params({id: 1})
  def not_authorized_helper(action_name, controller, action, params, specific_action = :get)
    # No sign in
    case(specific_action) # action and params
      when :get
        get action, params
      when :delete
        delete action, params
      when :post
        post action, params
      when :patch
        patch action, params
      else
        puts 'Extend test_not_authorized if method is missing'
    end
    assert_response 302 # redirected to sign in page
    assert_equal 'You need to sign in or sign up before continuing.', flash[:alert] # correct flash
  end
end


class ActionController::TestCase
  # added for access to devise helpers in all controller tests
  include Devise::TestHelpers
  # added for ability to use t() method for getting text from en.yml file
  include I18n

end

class ActionView::TestCase
  # added so that could test visual things in view tests that are dependent on current_user
  def current_user
    users(:correct)
  end
end

class ActionMailer::TestCase
  include ActionDispatch::Routing::UrlFor
  include Rails.application.routes.url_helpers
  include ActionView::Helpers
end

class ActionDispatch::IntegrationTest
  # Make the Capybara DSL available in all integration tests
  include Capybara::DSL

  # Reset sessions and driver between tests
  # Use super wherever this method is redefined in your individual test classes
  def teardown
    Capybara.reset_sessions!
    Capybara.use_default_driver
  end
end

